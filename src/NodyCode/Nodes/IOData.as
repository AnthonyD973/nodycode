package NodyCode.Nodes 
{
	/**
	 * ...
	 * @author Anthony DENTINGER
	 */
	public class IOData 
	{
		public var inputNumber:uint = 0;
		public var outputNumber:uint = 0;
		
		public function IOData(inputNumber:uint = 0, outputNumber:uint = 0)
		{
			this.inputNumber = inputNumber;
			this.outputNumber = outputNumber;
		}
		
	}

}