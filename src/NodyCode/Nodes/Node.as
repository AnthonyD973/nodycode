package NodyCode.Nodes 
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.utils.Input;
	import NodyCode.BuiltinFunctions;
	import NodyCode.Parser.Parser;
	import NodyCode.Properties;
	import Utils.MiscFunctions;
	import Utils.CollisionTypes;
	import Entities.MouseHitbox;
	import Worlds.Level;
	
	/**
	 * Defines nodes, the most important NodyCode object. Nodes can have values or expressions that can be evaluated at any given time.
	 * @author Anthony DENTINGER
	 */
	
	public class Node extends Entity {
		
		include "../../../embed/Includes/Denominations_Include_As_Static.as" //Allow access to Keywords, Operators, etc.
		
		/**
		 * Mouse position relative to this node.
		 */
		public var relativeMousePosition:Point;
		
		
		/**
		 * Object with following properties:
		 * 
		 * inputNumber:		Number of Inputs of node.
		 * outputNumber:	Number of Outputs of node.
		 */
		private var _IOData:Object = new IOData();
		
		
		private static const
		_OperatorObject:Object = { isOperator:true },
		_ArgumentObject:Object = { isOperator:false };
		
		
		/**
		 * Value of the node.
		 */
		public var value:*;
		

		/**
		 * Expression of the node put by the user.
		 */
		public var expression:String;
		
		
		/**
		 * Level object containing node instance.
		 */
		private var _levelPropObject:Properties;
		
		
		/**
		 * Node constructor. Superclass of other nodes.
		 * @param	_levelPropObject	Level object containing node instance.
		 * @param	x		x position.
		 * @param	y		y position.
		 * @param	IOData	Literal object defining the following properties:
		 * inputNumber:		Number of inputs of the Node.
		 * outputNumber:	Number of outputs of the Node.
		 */
		public function Node(levelPropObject:Properties, x:Number, y:Number, ioData:IOData) {
			this._levelPropObject = levelPropObject;
			
			this._IOData = ioData;
			
			super(x, y);
		}
		
		
		override public function update():void {
			//{--Allow mouse dragging
			if (collide(CollisionTypes.mouse, x, y)) {
				if (Input.mousePressed) {
					relativeMousePosition = new Point(Input.mouseX - x, Input.mouseY - y);
					MouseHitbox.dragging = this;
				}
				if (Input.mouseDown && MouseHitbox.dragging == this) {
					x = Input.mouseX - relativeMousePosition.x;
					y = Input.mouseY - relativeMousePosition.y;
				}
				if (Input.mouseReleased)
					MouseHitbox.dragging = null;
			}
			//}--
		}
		
		
		/**
		 * Generates a val array compatible with the [i]evaluate[/i] function and assigns it to the node's [i]value[/i] property.
		 * @param	expression	Input expression. A string.
		 */
		public function generateValueArray(expression:String):void {
			this.expression = expression;
			value = Parser.generateValArray(expression);
		}
		
		
		/**
		 * Evaluates the value of [i]this.value[/i].
		 * @param	val		Input assertion, in the form of a array with custom specs.
		 * @return	Result of assertion. Can be any data type.
		 */
		public function evaluate():* {
			return Parser.evaluate(value, _levelPropObject);
		}
		
		
	}

}