package NodyCode.Nodes 
{
	import Assets.BitmapAsset;
	import NodyCode.Nodes.Node;
	import net.flashpunk.utils.Input;
	import NodyCode.Properties;
	import Utils.CollisionTypes;
	/**
	 * ...
	 * @author Anthony DENTINGER
	 */
	public class IfNode extends Node
	{
		
		/**
		 * IF Node constructor.
		 * @param	x	x-position of node.
		 * @param	y	y-position of node.
		 */
		public function IfNode(levelPropObject:Properties, x:Number = 0, y:Number = 0) {
			super(levelPropObject, x, y, new IOData(1, 2));
			
			graphic = BitmapAsset.IFNodeBitmap;
			setHitbox(BitmapAsset.IFNodeBitmap.width, BitmapAsset.IFNodeBitmap.height);
		}
		
		override public function update():void
		{
			super.update();
		}
		
	}

}