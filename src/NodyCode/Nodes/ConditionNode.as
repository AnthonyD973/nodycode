package NodyCode.Nodes 
{
	import Assets.BitmapAsset;
	import NodyCode.Properties;
	import Utils.MiscFunctions;
	/**
	 * ...
	 * @author Anthony DENTINGER
	 */
	public class ConditionNode extends Node
	{
		include "/../../../embed/Includes/Denominations_Include_As_NonStatic.as"
		
		/**
		 * CONDITION Node constructor.
		 * @param	levelObject Level containing node instance.
		 * @param	x	x-position of node.
		 * @param	y	y-position of node.
		 */
		public function ConditionNode(levelPropObject:Properties, x:Number = 0, y:Number = 0) {
			super(levelPropObject, x, y, new IOData(0, 1) );
			
			graphic = BitmapAsset.CONDNodeBitmap;
			setHitbox(BitmapAsset.CONDNodeBitmap.width, BitmapAsset.CONDNodeBitmap.height);
		}
		
		
		override public function update():void {
			super.update();
		}
		
		
	}

}