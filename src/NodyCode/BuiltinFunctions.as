package NodyCode 
{
	import NodyCode.Classes.NCFunction;
	import NodyCode.Classes.NCList;
	import NodyCode.Classes.NCString;
	import NodyCode.Parser.FunctionVal;
	import NodyCode.Variable;
	import NodyCode.Nodes.Node;
	import Utils.MiscFunctions;
	import Worlds.Level;
	
	/**
	 * Defines generic NodyCode functions, e.g., the [i]coerce[/i] function.
	 * @author Anthony DENTINGER
	 */
	public class BuiltinFunctions {
		
		include "../../embed/Includes/Denominations_Include_As_Static.as"
		
		
		
		public function BuiltinFunctions() { }
		
		
		/**
		 * Implicit coercion of [i]obj[/i], a literal to [i]typeToCoerce[/i]. If successful, returns the coerced object. Else, returns null.
		 * 
		 * If obj is a string representing a variable/funtion, gets its value from [i]property[/i].
		 * @param	obj	Object to be coerced.
		 * @param	typeToCoerce	Type to coerce [i]obj[/i] to.
		 * @param	property [i]Properties[/i] object to get the value of [i]obj[/i], in case it is a string representing a var/fn.
		 * @return	Coerced object, or null object if cannot be coerced.
		 */
		public static function coerce(obj:*, typeToCoerce:String, property:Properties = null):* {
			
			/**
			 * Type of the input [i]obj[/i].
			 */
			var typeofObj:String;
			
			/**
			 * Holds the value, a literal, to coerce.
			 */
			var obj_2:*;
			
			//{--If obj is a var/fn, take its value and type.
			if (obj is String && MiscFunctions.isIn(obj.charAt(0), IDENTIFIERS)) {
				typeofObj = property[obj.substring(1, obj.length - 1)].type;
				obj_2 = property[obj.substring(1, obj.length - 1)].value;
			}
			//}--
			
			//{--Else, obj is a literal. Get its type.
			else {
				obj_2 = obj;
				
				typeofObj = getType(obj);
			}
			//}--
			
			//{--If nothing needs to be done, do nothing. Return 'obj_2'.
			if (typeofObj == typeToCoerce) {
				return obj_2;
			}
			//}--
			
			//{--Perform the coercion, if possible.
			switch (typeToCoerce) {
				
				//{--Coerce to int.
				case Int: {
					//{--Coerce from Bool to Int. Same thing as for coercing from Bool to Float.
					if (typeofObj == Bool) {
						
						if (obj_2 == false) {
							return 0;
						}
						else {
							return 1;
						}
						
						break;
					}
					//}--
					
					break;
				}
				//}--
				
				//{--Coerce to float.
				case Float: {
					//{--Coerce from Bool to Float. Same thing as for coercing from Bool to Int.
					if (typeofObj == Bool) {
						
						if (obj_2 == false) { //Boolean 'false' corresponds to float '0'
							return 0;
						}
						else { //Boolean 'true' corresponds to float '1'
							return 1;
						}
						break;
					}
					//}--
					
					break;
				}
				//}--
				
				//{--Coerce to boolean.
				case Bool: {
					switch (typeofObj) {
						case Int: { //Coerce from Int to Bool. Same thing as coercing from Float to Bool.
							if (obj_2 == 0) {
								return false; //Float '0' corresponds to Boolean 'false'.
							}
							if (obj_2 == 1) {
								return true; //Float '1' corresponds to Boolean 'true'.
							}
							
							break;
						}
						
						case Float: { //Coerce from Float to Bool. Same thing as coercing from Int to Bool.
							if (obj_2 == 0) {
								return false; //Float '0' corresponds to Boolean 'false'.
							}
							if (obj_2 == 1) {
								return true; //Float '1' corresponds to Boolean 'true'.
							}
							
							break;
						}
						
					}
					
					break;
				}
				//}--
				
				//{--Nothing can be coerced to Str,  so do nothing in that case.
				case Str: {
					break;
				}
				//}--
				
				//{--Nothing can be coerced to List, so do nothing in that case.
				case List: {
					break;
				}
				//}--
				
				
				default: {
					throw(new Error('Coercion to an unknown type: "' + typeToCoerce + '"'));
					break;
				}
			}
			//}--
			
			//{--If obj has not been coerced, return null.
			return null;
			//}--
			
		}
		
		
		/**
		 * Explicit coercion of [i]obj[/i] to an int. Raises an error if coercion is impossible.
		 * @param	obj	Input Object to be coerced.
		 * @param	property [i]Properties[/i] object to get the value of [i]obj[/i], in case it is a string representing a var/fn.
		 * @return	Coerced object.
		 */
		public static function toInt(obj:*, property:Properties = null):* {
			/**
			 * Type of the input [i]obj[/i].
			 */
			var typeofObj:String;
			
			/**
			 * Holds the value, a literal, to coerce.
			 */
			var obj_2:*;
			
			//{--If obj is a var/fn object, get its value and type.
			if (obj is String && MiscFunctions.isIn(obj.charAt(0), IDENTIFIERS)) {
				typeofObj = property[obj.substring(1, obj.length - 1)].type;
				obj_2 = property[obj.substring(1, obj.length - 1)].value;
			}
			//}--
			
			//{--Else, obj is a literal. Get its type.
			else {
				obj_2 = obj;
				typeofObj = getType(obj);
			}
			//}--
			
			//{--Else, attempt to perform the coercion.
			switch(typeofObj) {
				case Int: { //Coerce from Int to Int.
					return obj_2;
					
					break;
				}
				
				case Float: { //Coerce from Float to Int.
					return int(obj_2);
					
					break;
				}
				
				case Bool: { //Coerce from Bool to Int.
					if (obj_2 == false) {
						return 0; //Bool 'false' corresponds to int '0'.
					}
					else {
						return 1; //Bool 'true' correspoinds to int '1'.
					}
					
					break;
				}
				
				case Str: { //Coerce from Str to Int.
					//{--If obj can be coerced to an float (and thus to an int), then do so.
					if(!isNaN(Number(obj_2))) {
						return int(obj_2);
					}
					//}--
					
					//{--Allow the NaN number.
					else if (obj_2 == Nan) {
						return Nan;
					}
					//}--
					
					break;
				}
				
			}
			//}--
			
			
			/**
			 * ____________________________________________________________________________________
			 * |Need to add more information for user in error below, e.g. at what node it occurs.|
			 * ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
			 */
			
			//{--If coercion was not possible, throw an error.
			throw(new Error("Convertion of " + MiscFunctions.displayString(obj_2) + " to " + Int + " is impossible."));
			//}--
			
		}
		
		/**
		 * Explicit coercion of [i]obj[/i] to a float. Raises an error if coercion is impossible.
		 * @param	obj	Input Object to be coerced.
		 * @param	property [i]Properties[/i] object to get the value of [i]obj[/i], in case it is a string representing a var/fn.
		 * @return	Coerced object.
		 */
		public static function toFloat(obj:*, property:Properties = null):* {
			/**
			 * Type of the input [i]obj[/i].
			 */
			var typeofObj:String;
			
			/**
			 * Holds the value, a literal, to coerce.
			 */
			var obj_2:*;
			
			//{--If obj is a var/fn object, get its value and type.
			if (MiscFunctions.isIdentifierName(obj)) {
				typeofObj = property[obj.substring(1, obj.length)].type;
				obj_2 = property[obj.substring(1, obj.length)].value;
			}
			//}--
			
			//{--Else, obj is a literal. Get its type.
			else {
				obj_2 = obj;
				typeofObj = getType(obj);
			}
			//}--
			
			//{--Attempt to perform the coercion.
			switch (typeofObj) {
				case Int: { //Coerce from Int to Float. Nothing needs to be done.
					return obj_2;
				}
				
				case Float: { //Coerce from Float to Float. Nothing needs to be done.
					return obj_2;
				}
				
				case Bool: { //Coerce from Bool to Float.
					if (obj_2 == false) {
						return 0; //Bool 'false' corresponds to float '0'
					}
					else {
						return 1; //Bool 'true'  corresponds to float '1'
					}
					
					break;
				}
				
				case Str: { //Coerce from String to Float.
					//{--If obj_2 can be coerced to an float, then do so.
					if (!isNaN(Number(obj_2))) {
						return Number(obj_2);
					}
					//}--
					
					//{--Allow the NaN number.
					else if (obj_2 == Nan) {
						return null;
					}
					//}--
					
					break;
				
				}
				
			}
			//}--
			
			/**
			 * ____________________________________________________________________________________
			 * |Need to add more information for user in error below, e.g. at what node it occurs.|
			 * ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
			 */
			
			//{--If coercion was not possible, throw an error.
			throw(new Error('Convertion of "' + MiscFunctions.displayString(obj_2) + '" to ' + Float + " is impossible."));
			//}--
			
			return false;
		}
		
		/**
		 * Explicit coercion of [i]obj[/i] to a boolean. Raises an error if coercion is impossible.
		 * @param	obj	Input Object to be coerced.
		 * @param	property [i]Properties[/i] object to get the value of [i]obj[/i], in case it is a string representing a var/fn.	
		 * @return	Coerced object.
		 */
		public static function toBool(obj:*, property:Properties = null):Boolean {
			/**
			 * Type of the input [i]obj[/i].
			 */
			var typeofObj:String;
			
			/**
			 * Holds the value, a literal, to coerce.
			 */
			var obj_2:*;
			
			//{--If obj is a var/fn object, get its value and type.
			if (MiscFunctions.isIdentifierName(obj)) {
				typeofObj = property[obj.substring(1, obj.length - 1)].type;
				obj_2 = property[obj.substring(1, obj.length - 1)].value;
			}
			//}--
			
			//{--Else, obj is a literal. Get its type.
			else {
				obj_2 = obj;
				typeofObj = getType(obj);
			}
			//}--
			
			//{--Else, attempt to perfrom the coercion.
			switch (typeofObj) {
				case Bool: { //Coerce from Bool to Bool. Nothing needs to be done.
					return obj_2;
				}
				
				case Int: { //Coerce from Int to Bool.
					if (obj_2 == 0) {
						return false; //Int '0' corresponds to Bool 'false'.
					}
					else if (obj_2 == 1) {
						return true; //Int '1' corresponds to Bool 'true'.
					}
					
					break;
				}
				
				case Float: { //Coerce from Float to Bool.
					if (obj_2 == 0) {
						return false; //Int '0' corresponds to Bool 'false'.
					}
					else if (obj_2 == 1) {
						return true; //Int '1' corresponds to Bool 'true'.
					}
					
					break;
				}
				
				case Str: { //Coerce from String to Bool.
					if (obj_2 == False || toFloat(obj_2, null) === 0) { //Strict equality, since toFloat function returns false when coercion is impossible.
						return false;
					}
					else if (obj_2 == True || toFloat(obj_2, null) === 1) {
						return true;
					}
					
					break;
				}
				
			}
			//}--
			
			/**
			 * ____________________________________________________________________________________
			 * |Need to add more information for user in error below, e.g. at what node it occurs.|
			 * ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
			 */
			
			//{--If coercion was not possible, throw an error.
			throw(new Error("Convertion of " + MiscFunctions.displayString(obj_2) + " to " + Bool + " is impossible."));
			//}--
			
		}
		
		/**
		 * Explicit coercion of [i]obj[/i] to a string using the [i]MiscFunction.displayString[/i] function. Raises an error if coercion is impossible.
		 * @param	obj	Input object to be coerced.
		 * @param	property [i]Properties[/i] object to get the value of [i]obj[/i], in case it is a string representing a var/fn.	
		 * @return	Coerced object.
		 */
		public static function toStr(obj:*, property:Properties = null):String {
			/**
			 * Holds the value, a literal, to coerce.
			 */
			var obj_2:*;
			
			//{--If obj is a string representing a var/fn, get its value from property.
			if (MiscFunctions.isIdentifierName(obj)) {
				obj_2 = property[obj.substring(1, obj.length - 1)].value;
			}
			//}--
			
			//{--Else, obj is a literal. Get its type.
			else {
				obj_2 = obj;
			}
			//}--
			
			return MiscFunctions.displayString(obj_2);
		}
		
		
		
		/**
		 * Gets the value of [i]obj[/i], taking into account whether [i]obj[/i] is a literal value or a variable/function.
		 * @param	obj	Input object.
		 * @param	property [i]Properties[/i] object to get the value of [i]obj[/i], in case it is a string representing a var/fn.	
		 * @return	[i]obj[/i] if [i]obj[/i] is not a variable/function. Else, get the value of the variable/function in [i]this.property[/i] and return it.
		 */
		public static function valueOf (obj:*, propObject:Properties):* { //+++
			//{--If object is a literal value, return it.
			if (!MiscFunctions.isIdentifierName(obj)) {
				if (MiscFunctions.isNodyCodeClass(obj)) {
					return obj.evaluate(propObject);
				}
				return obj;
			}
			//}--
			
			//{--Else, if it is an existing variable/function, return its value.
			if (MiscFunctions.isIdentifierName(obj) && propObject[obj.substring(1, obj.length)] != null) {
				return valueOf(propObject[obj.substring(1, obj.length)].value, propObject);
			}
			//}--
			
			//{--Else, throw an error
			throw(new Error('The variable/function named "' + obj.substring(1, obj.length) + '" does not exist.'));
			//}--
			
		}
		
		
		/**
		 * Decides of what type [i]obj[/i] is.
		 * E.g. : 2 is "int", 2.1 is "float", "Hello World!" is "str". See Utils.Denominations for data types.
		 * @param	value	Input value to get the valueType of.
		 * @param	property	[i]Properties[/i] object to get the type of [i]obj[/i], in case it is a string representing a var/fn.
		 * @return	Type of value. A string.
		 */
		public static function getType(obj:*, property:Properties = null):String {
			//{--If value is a literal, get its type
			if (obj is int) {
				return Int;
			}
			if (obj is Number || obj == Nan) {
				return Float;
			}
			if (obj is Boolean) {
				return Bool;
			}
			if (obj is NCString) {
				return Str;
			}
			if (obj is NCList) {
				return List;
			}
			//}--
			
			//{--Else, if it is an existing var/fn object, return its type.
			if (MiscFunctions.isIdentifierName(obj) && property[obj.substring(1, obj.length)]) {
				return property[obj.substring(1, obj.length)].type;
			}
			//}--
			
			throw(new Error("Data type of value is unknown: " + MiscFunctions.displayString(obj)));
		}
		
	}

}