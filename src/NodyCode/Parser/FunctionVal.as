package NodyCode.Parser 
{
	import NodyCode.BuiltinFunctions;
	import NodyCode.Properties;
	import Utils.Denominations;
	import Utils.MiscFunctions;
	/**
	 * ...
	 * @author Anthony DENTINGER
	 */
	public class FunctionVal 
	{
		private var
		ArgumentSeparator:String = Denominations.ArgumentSeparator,
		QUOTES:Array = Denominations.QUOTES;
		
		/**
		 * Name of the called function.
		 */
		public var functionName:String;
		
		/**
		 * Arguments of the call.
		 */
		public var argArray:Array;
		
		
		/**
		 * Parses [i]functionName[/i]'s arguments using recursions of [i]Parser.generateValArray[/i].
		 * @param	functionName	Function's name.
		 * @param	args	Arguments. A string.
		 */
		public function FunctionVal(functionName:String = "", args:String = "") {
			//MiscFunctions.display("FunctionVal constructor called.   functionName:", functionName, "  &&   args:", args);
			
			if (arguments.length == 0) {
				return;
			}
			if (args == "") {
				this.functionName = functionName;
				this.argArray = [];
				return;
			}
			
			var tempArray:Array = [];
			var startIndex:int = 0;
			
			for (var i:int = 0; i < args.length; i++) {
				if (MiscFunctions.isIn(args.charAt(i), QUOTES)) {
					i = MiscFunctions.getStringEndIndex(args, i);
					continue;
				}
				
				if (args.charAt(i) == ArgumentSeparator) {
					tempArray.push(args.substring(startIndex, i));
					startIndex = i + 1;
				}
			}
			tempArray.push(args.substring(startIndex));
			
			
			/**
			 * Arguments of the function.
			 */
			var argArray:Array = [];
			
			for each(var arg:String in tempArray) {
				var tempArgArray:* = Parser.generateValArray(arg);
				argArray.push(tempArgArray is Array && tempArgArray.length == 1 ? tempArgArray[0] : tempArgArray);
			}
			
			this.functionName = functionName;
			
			//MiscFunctions.display("argArray[0]: '" + argArray[0] + "'", argArray[0] === "");
			this.argArray = (argArray.length == 1 && argArray[0] === "") ? [] : argArray; //Note to self: the strict equality "===" is to avoid confusion between the Number 0 and the String "" : (0 == "") returns true, but (0 === "") is false.
			MiscFunctions.display("returned FunctionVal object:", this);
		}
		
		
		/**
		 * Evaluates the value of [i]this[/i] during runtime.
		 * @param	propObject	Object to get name values from.
		 * @return	A FunctionVal object whose value is the current value of [i]this[/i].
		 */
		public function evaluate(propObject:Properties):FunctionVal {
			var fv:FunctionVal = new FunctionVal(functionName);
			
			if (argArray.length != 0) {
				for (var i:int = 0; i < argArray.length; i++) {
					var tempArray:* = Parser.checkArray(argArray[i], propObject);
					fv.argArray.push(tempArray);
					MiscFunctions.display("fv.evaluate[", argArray[i], "] =", tempArray);
				}
			}
			
			return fv;
		}
		
	}

}