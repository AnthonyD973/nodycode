package NodyCode.Parser 
{
	import NodyCode.BuiltinFunctions;
	import NodyCode.Classes.NCList;
	import NodyCode.Classes.NCString;
	import Utils.Denominations;
	import Utils.MiscFunctions;
	
	/**
	 * An all-static class containing classes' methods.
	 * @author Anthony DENTINGER
	 */
	
	public class ClassMethods {
		
		public static var
		Args:String = Denominations.Args;
		
		private static var
		AS3Classes:Array = [int, Number, NCString, NCList, Boolean],
		NCClasses:Array = [Denominations.Int, Denominations.Float, Denominations.Str, Denominations.List, Denominations.Bool];
		
		
		public function ClassMethods() { }
		
		/**
		 * 
		 * @param	obj
		 * @param	methodName
		 * @param	argArray
		 * @return
		 */
		public static function dispatchMethod(obj:*, methodName:String, argArray:Array):* {
			try {
				
				//{---Do the type checks
				//{--Do the type checks for required params
				var numRequiredParams:int = Math.min(obj.methodTypes[methodName][obj.methodTypes[methodName].length - 2] == Args ? obj.methodTypes[methodName].length - 2 : obj.methodTypes[methodName].length - 1, argArray.length);
				
				for (var i:int = 0; i < numRequiredParams; i++) {
					MiscFunctions.display("Doing required type check. argArray[i]:", argArray[i], " //  AS3 class required:", obj.methodTypes[methodName][i], " //  passes the test?", argArray[i] is obj.methodTypes[methodName][i]);
					
					if (!(argArray[i] is obj.methodTypes[methodName][i])) {
						throw new Error('TypeError: Cannot convert from \'' + BuiltinFunctions.getType(argArray[i]) + '\' to \'' + NCClasses[AS3Classes.indexOf(obj.methodTypes[methodName][i])] + '\' implicitly', 9000);
					}
				}
				//}--
				
				//{--Do the type checks for optional params
				var numOptionalParams:int = Math.min(obj.methodTypes[methodName][obj.methodTypes[methodName].length - 1].length, argArray.length - numRequiredParams);
				
				for (var i:int = 0; i < numOptionalParams; i++) {
					MiscFunctions.display("Doing optional type check. argArray[i]:", argArray[i + numRequiredParams], " //  AS3 class required:", obj.methodTypes[methodName][obj.methodTypes[methodName].length - 1][i], " //  passes the test?", argArray[i + numRequiredParams] is obj.methodTypes[methodName][obj.methodTypes[methodName].length - 1][i]);
					
					if (!(argArray[i + numRequiredParams] is obj.methodTypes[methodName][obj.methodTypes[methodName].length - 1][i])) {
						throw new Error('TypeError: Cannot convert from \'' + BuiltinFunctions.getType(argArray[i + numRequiredParams]) + '\' to \'' + NCClasses[AS3Classes.indexOf(obj.methodTypes[methodName][i])] + '\' implicitly', 9000);
					}
				}
				//}--
				//}---
				
				MiscFunctions.display("*** Applying '", BuiltinFunctions.getType(obj), "' method named '", methodName, "' with arguments:", argArray, "***");
				
				return obj[methodName].apply(obj, argArray);
			}
			catch (e:Error) {
				
				switch (e.errorID) {
					//{--If the number of arguments is wrong.
					case 1063: {
						if (argArray.length < obj.methodTypes[methodName].length - 1) {
							throw new Error('ArgumentError: \'' + BuiltinFunctions.getType(obj) + '\' method named \'' + methodName + '\' has been given ' + argArray.length + ' ' + (argArray.length == 1 ? 'argument' : 'arguments') + ', while ' + (obj.methodTypes[methodName].length - 1) + ' ' + (obj.methodTypes[methodName].length - 1 == 1 ? 'was' : 'were') + ' required', 1063);
						}
						else if (obj.methodTypes[methodName][obj.methodTypes[methodName].length - 2] != Args && argArray.length > obj.methodTypes[methodName].length - 1 + obj.methodTypes[methodName][obj.methodTypes[methodName].length - 1].length) {
							throw new Error('ArgumentError: \'' + BuiltinFunctions.getType(obj) + '\' method named \'' + methodName + '\' has been given ' + argArray.length + ' ' + (argArray.length == 1 ? 'argument' : 'arguments') + ', while ' + (obj.methodTypes[methodName].length - 1 + obj.methodTypes[methodName][obj.methodTypes[methodName].length - 1].length) + ' at most ' + (obj.methodTypes[methodName].length - 1 + obj.methodTypes[methodName][obj.methodTypes[methodName].length - 1].length == 1 ? 'was' : 'were') + ' required.', 1063);
						}
						
						break;
					}
					//}--
					
					//{--If one of the parameters has the wrong type.
					case 9000: {
						throw e;
						
						break;
					}
					//}--
					
					//{--If one of the parameters has the wrong type. Normally, this error should trigger in the Try structure, but leaving this just in case.
					case 1034: {
						for (var i:int = 0; i < obj.methodTypes[methodName].length; i++) {
							if (obj.methodTypes[methodName][i] != BuiltinFunctions.getType(argArray[i])) {
								throw new Error('TypeError: Cannot convert from \'' + BuiltinFunctions.getType(argArray[i]) + '\' to \'' + obj.methodTypes[methodName][i] + '\' implicitly', 9000);
							}
						}
						
						break;
					}
					//}--
					
					//{--If methodName is not a method of obj
					case 1010: {
						throw new Error('AttributeError: \'' + BuiltinFunctions.getType(obj) + '\' method named \'' + methodName + '\' does not exist');
						
						break;
					}
					//}--
					
					default: {
						throw e;
						
						break;
					}
				}
				
			}
		}
		
		
		private static const StringMethods:Object = {
			split:	function(separator:NCString):NCList							{ return this.split(separator); },
			join:	function(list:NCList):NCString								{ return this.join(list); },
			lower:	function():NCString											{ return this.lower(); },
			upper:	function():NCString											{ return this.upper(); },
			index:	function(substr:NCString, startIndex:int = 0):uint			{ return this.index(substr, startIndex); },
			rindex:	function(substr:NCString, startIndex:int = 0x7fffffff):uint	{ return this.rindex(substr, startIndex); }
		};
		
		
		private static const ListMethods:Object = {
			append:	function(...args):void								{ return this.append(args); },
			pop:	function(index:int = 0x7fffffff):*					{ return this.pop(index); },
			reverse:function():void										{ return this.reverse(); },
			index:	function(obj:*, startIndex:int = 0):uint			{ return this.index(obj, startIndex); },
			rindex:	function(obj:*, startIndex:int = 0x7fffffff):uint	{ return this.rindex(obj, startIndex); }
		};
		
	}
}