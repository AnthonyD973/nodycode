package NodyCode.Parser 
{
	import NodyCode.Properties;
	import Utils.MiscFunctions;
	import NodyCode.BuiltinFunctions;
	import NodyCode.Classes.NCList;
	import NodyCode.Classes.NCString;
	
	/**
	 * All-static class containing NodyCode's parsing functions.
	 * 
	 * Class contains two public functions:
	 * 1) [i]geverateValueArray[/i]: parses a line of code.
	 * 2) [i]evaluate[/i]: executes a parsed line of code, and returns its value, if case applies.
	 * 
	 * @author Anthony DENTINGER
	 */
	
	public class Parser
	{
		
		include "../../../embed/Includes/Denominations_Include_As_Static.as" //Allow access to Keywords, Operators, etc.
		
		
		private static const
		_OperatorObject:Object = { isOperator:true },
		_ArgumentObject:Object = { isOperator:false };
		
		
		public function Parser() { }
		
		
		/**
		 * ___________________________
		 * | Generation of Val Array |
		 * ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
		 */
		
		/**
		 * Generates an expression array compatible with the [i]evaluate[/i] function.
		 * @param	expression	Input expression. A string.
		 * @return The compatible array.
		 */
		public static function generateValArray(expression:String):* {
			/**
			 * Array containing:
			 * 0 - The [i]val[/i] array, which is the expression being parsed
			 * 1 - The [i]flagArray[/i] array, in which each element flagArray[j] is
			 * an object containing a boolean property called [i]isOperator[/i]
			 * which informs whether val[j] is an operator or not.
			 */
			var tempArray:Array;
			
			expression = removeSpaces(expression);
			
			tempArray = splitOperatorsAndArgs(expression);
			//MiscFunctions.display("val:", tempArray[0], "     flagArray:", tempArray[1]);
			
			//--See Python's operator precedence at https://docs.python.org/2/reference/expressions.html#operator-precedence
			//Most precedent operators first, least precedent after.
			tempArray = checkOperator(tempArray[0], tempArray[1], [Power]);
			
			tempArray = checkOperator(tempArray[0], tempArray[1], [Positive, Negative]);
			
			tempArray = checkOperator(tempArray[0], tempArray[1], [Multiply, Divide, Modulo, FloorDivision]);
			
			tempArray = checkOperator(tempArray[0], tempArray[1], [Add, Subtract]);
			
			tempArray = checkOperator(tempArray[0], tempArray[1], [Equals, DifferentFrom, LowerOrEqualTo, GreaterOrEqualTo, LowerThan, GreaterThan]);
			
			tempArray = checkOperator(tempArray[0], tempArray[1], [Not]);
			
			tempArray = checkOperator(tempArray[0], tempArray[1], [And, Nand]);
			
			tempArray = checkOperator(tempArray[0], tempArray[1], [Or, Xor, Nor, Xnor]);
			
			tempArray = checkOperator(tempArray[0], tempArray[1], [Assign]);
			
			tempArray = checkOperator(tempArray[0], tempArray[1], [Return]);
			//--
			
			if (tempArray[0].length == 1 && (tempArray[0][0] is Array)) {
				tempArray[0] = tempArray[0][0];
			}
			
			return tempArray[0].length == 1 ? makeLiteral(tempArray[0][0]) : tempArray[0];
		}
		
		
		/**
		 * Removes all spaces that have no use in expression:
		 * 
		 * First removes multiple adjacent spaces, and, of the remaining spaces, removes the useless ones.
		 * @param	expression	Input expression.
		 * @return	expression, with adjacent spces removed.
		 */
		private static function removeSpaces(expression:String):String {
			//{--Remove tabulations and replace them by spaces.
			expression = expression.split("\t").join(" ");
			//}--
			
			for (var i:int = 0; i < expression.length; i++) {
				//{--If the i-th character is a string-opening character, assign i to the string's end and continue.
				if (MiscFunctions.isIn(expression.charAt(i), QUOTES)) {
					i = MiscFunctions.getStringEndIndex(expression, i);
					continue;
				}
				//}--
				
				if (expression.charAt(i) == " ") {
					/**
					 * Holds the index of end of the substring of [i]expression[/i] that contains spaces, plus 1.
					 */
					var endIndex:int = i + 1;
					while (expression.charAt(endIndex) == " ") {
						endIndex++;
					}
					
					if (endIndex != i + 1) {
						expression = expression.substring(0, i + 1) + expression.substring(endIndex);
					}
				}
			}
			
			/**
			 * Holds whether the preceding character is part of the name of a var/function.
			 */
			var identifierFlag:Boolean = false;
			
			for (var i:int = 0; i < expression.length; i++) {
				//{--If the i-th character is a string-opening character, assign i to the string's end and continue.
				if (MiscFunctions.isIn(expression.charAt(i), QUOTES)) {
					i = MiscFunctions.getStringEndIndex(expression, i);
					continue;
				}
				//}--
				
				//{--If the i-th character of 'expression' is a space, then only remove that space if removing it will not change the name of the token before that space. I.e., if: the character before is not part of the name of a var/fn OR the character after it is not.
				if (expression.charAt(i) == " ") {
					if (!identifierFlag || !MiscFunctions.isIn(expression.charAt(i + 1), VarChars)) {
						expression = expression.substring(0, i) + expression.substring(i + 1);
						identifierFlag = false;
						i--;
					}
				}
				//}--
				
				if (!identifierFlag && MiscFunctions.isIn(expression.charAt(i), VarChars)) {
					identifierFlag = true;
				}
				else if (identifierFlag && !MiscFunctions.isIn(expression.charAt(i), VarChars)) {
					identifierFlag = false;
				}
			}
			
			return expression;
			
		}
		
		
		/**
		 * Splits [i]expression[/i] in an array of substrings of it, each containing either an operator or an argument. Recursively calls [i]generateValArray[/i] to respect parenthesis precedence. Raises an error when one is necessary.
		 * 
		 * For instance, given [i]expression[/i] = "2.5 + (i% (4 - 1)) == not False or foo(param)!= None", the array returned is:
		 * ["2.5", "+", ["ヘi", "%", [4, "-", 1]], "==", "not", "False", "or", ["ベfoo", "ヘparam"], "!=", "None"].
		 * @param	expression	Expression string.
		 * @return	Array containing substrings, which, in turn, contain operators and their arguments. Variables and Functions are inserted with a special aribtrary character, which we will call [i]identifier[/i] as defined in [i]Utils.Denominations[/i].
		 */
		private static function splitOperatorsAndArgs(expression:String):Array {
			/**
			 * Array that will be returned.
			 */
			var val:Array = [];
			/**
			 * Array with associative subarrays that contain the following boolean properties about the [i]val[/i] array: {[i]isFinal[/i], [i]isOperator[/i]}.
			 * 
			 * -[i]isFinal[/i]: whether this element of [i]val[/i] has already been parsed and should be treated as a black box.
			 * -[i]isOperator[/i]: whether the element contains only an operator.
			 */
			var flagArray:Array = [];
			/**
			 * Holds the index of beginning of leftmost part of [i]expression[/i] that has not yet been copied in val.
			 */
			var startString:int = 0;
			
			
			/**
			 * Holds the character with which the current string literal was opened, when appropriate.
			 */
			var stringOpenedWith:String;
			
			MiscFunctions.display("splitOperatorsAndArgs's input: ".toUpperCase(), expression);
			
			//{--For each character in expression, check if it is the beginning of an operator, keyword, variable or function.
			Loop1: for (var i:int = 0; i < expression.length; i++) {
				
				MiscFunctions.display("-- i =", i, "--");
				//{--If starting a number, go to that number's end.
				if (MiscFunctions.isIn(expression.charAt(i), NumberSymbols) && (expression.charAt(i) != DecimalPoint || (i <= expression.length - 2 && MiscFunctions.isIn(expression.charAt(i + 1), Digits)))) {
					i = MiscFunctions.getNumberEndIndex(expression, i);
					
					continue;
				}
				//}--
				MiscFunctions.display("Not a number.", val);
				//{--If opening a string, assign i the value of end of the string.
				if (MiscFunctions.isIn(expression.charAt(i), QUOTES)) {
					MiscFunctions.display("QUOTE found. New value of i:", MiscFunctions.getStringEndIndex(expression, i), "      ", expression);
					i = MiscFunctions.getStringEndIndex(expression, i);
					continue;
				}
				//}--
				MiscFunctions.display("Not a string.", val);
				//{--If opening a nestable argument (e.g., a list or a bracket), place i at the end of the nestable and throw an error when something is wrong.
				if (MiscFunctions.isIn(expression.charAt(i), NESTABLEOPEN)) {
					MiscFunctions.display("NESTABLE found. New value of i:", MiscFunctions.getNestableEndIndex(expression, i), "      ", expression);
					i = MiscFunctions.getNestableEndIndex(expression, i);
					continue;
				}
				//}--
				MiscFunctions.display("Not a nestable.", val);
				//{--If closing a nestable argument whilst none was open, throw an error.
				if (MiscFunctions.isIn(expression.charAt(i), NESTABLECLOSE)) {
					MiscFunctions.display("CLOSING NESTABLE. Throwing error.", "      ", expression);
					switch(expression.charAt(i)) {
						case BracketClose: {
							throw(new Error('Invalid syntax: A bracket was closed but not opened.'));
							break;
						}
						case ParenthesisClose: {
							throw(new Error('Invalid syntax: A parenthesis was closed but not opened.'));
							break;
						}
						
						default: {
							throw(new Error('To dev: Recently added a new type of nestable argument, but did not implement it in NodyCode.Parser.Parser.splitOperatorsAndArgs: "' + expression.charAt(i) + '"'))
							break;
						}
					}
					
				}
				//}--
				MiscFunctions.display("Not closing nestable.", val);
				
				//{--If starting a name, check whether it is a keyword operator, a keyword literal or a var/fn name.
				if (MiscFunctions.isIn(expression.charAt(i), VarBeginChars)) {
					MiscFunctions.display("NAME found.", "      ", expression);
					/**
					 * Index of end of token.
					 */
					var nameEndIndex:int = MiscFunctions.getIdentifierEndIndex(expression, i);
					
					//{--If the token is a binary keyword operator, expect a left argument.
					if (MiscFunctions.isIn(expression.substring(i, nameEndIndex + 1), BinaryKeywordOperators)) {
						val.push(parseArg(expression.substring(startString, i)), expression.substring(i, nameEndIndex + 1));
						flagArray.push(_ArgumentObject, _OperatorObject);
						
						i = nameEndIndex;
						startString = nameEndIndex + 1;
						continue;
					}
					//}--
					
					//{--Else, if the token is a unary keyword operator, expect no left argument.
					if (MiscFunctions.isIn(expression.substring(i, nameEndIndex + 1), UnaryKeywordOperators)) {
						//{--A unary operator should have no argument on its left.
						if ((i != startString && expression.charAt(i) != " ") || i > startString + 1) {
							throw(new Error('Invalid syntax: The operator "' + expression.substr(i, Operators[j].length) + '" cannot have an argument on its left.'));
						}
						//}--
						
						val.push(expression.substring(i, nameEndIndex + 1));
						flagArray.push(_OperatorObject);
						
						i = nameEndIndex;
						startString = nameEndIndex + 1;
						continue;
					}
					//}--
					
					//{--Else, if the token is a literal, do not do anything: arguments should not be parsed yet.
					if (MiscFunctions.isIn(expression.substring(i, nameEndIndex + 1), KeywordLiterals)) {
						i = nameEndIndex;
						continue;
					}
					//}--
					
					//{--Else, if token has been called as a function (i.e., the call "()" operator was used), add a FUNCTION character.
					else if (expression.charAt(nameEndIndex + 1) == ParenthesisOpen) {
						expression = expression.substring(0, i) + FUNCTION + expression.substring(i);
						i = nameEndIndex + 1;
						continue;
					}
					//}--
					
					//{--Else, add a VARIABLE character.
					else {
						expression = expression.substring(0, i) + VARIABLE + expression.substring(i);
						i = nameEndIndex + 1;
						continue;
					}
					//}--
					
				}
				//}--
				MiscFunctions.display("Not a name.", val);
				//{--Check if it is an operator that is not a keyword.
				Loop2: for (var j:int = 0; j < NonKeywordOperators.length; j++) {
					//{--If the expression is too short to contain this operator, continue to the next operator.
					if (i + NonKeywordOperators[j].length > expression.length) {
						continue;
					}
					//}--
					
					//{--If characters of expression and NonKeywordOperators[j] are different, then continue on to the next possible operator.
					for (var k:int = 0; k < NonKeywordOperators[j].length; k++) {
						if (expression.charAt(i + k) != NonKeywordOperators[j].charAt(k)) {
							continue Loop2;
						}
					}
					//}--
					
					//{----If the expression's substring is an operator, add it and its argument in val.
					MiscFunctions.display("OPERATOR found.", "      ", expression);
					//{--If the operator is binary, expect 2 arguments.
					if (MiscFunctions.isIn(NonKeywordOperators[j], BinaryOperators)) {
						val.push(parseArg(expression.substring(startString, i)), NonKeywordOperators[j]);
						flagArray.push(_ArgumentObject, _OperatorObject);
					}
					//}--
					
					//{--Else, the operator is unary. Expect only 1 argument.
					else {
						//{--A unary operator should have no argument on its left.
						if (i != startString) {
							throw(new Error('Invalid syntax. The operator "' + expression.substr(i, NonKeywordOperators[j].length) + '" cannot have an argument on its left.'));
						}
						//}--
						
						val.push(NonKeywordOperators[j]);
						flagArray.push(_OperatorObject);
					}
					//}--
					
					i += NonKeywordOperators[j].length - 1; //Do not look for operators inside the operator we just found.
					startString = i + 1;
					continue Loop1;
					//}----
					
				}
				//}--
				MiscFunctions.display("Not a keyword OP.", val);
			}
			//}--
			
			//{--Add in 'val' what is left of 'expression' that has not been copied in 'val'.
			val.push(parseArg(expression.substring(startString)));
			flagArray.push(_ArgumentObject);
			//}--
			
			
			//{--Remove spaces from the string
			for (var k:int = val.length - 1; k >= 0; k--) {
				//{--Only look in Strings for those spaces.
				if (!(val[k] is String) || MiscFunctions.isIn(val[k].charAt(0), QUOTES)) {
					continue;
				}
				//}--
				
				//{--Proceed to removing any space found.
				for (var l:int = 0; l < val[k].length; l++) {
					if (val[k].charAt(l) == " ") {
						val[k] = val[k].substring(0, l) + val[k].substring(l + 1);
					}
				}
				//}--
				
			}
			//}--
			
			//{--Remove null strings in val.
			for (var l:int = val.length - 1; l >= 0; l--) {
				if (val[l] is String && val[l].length == 0) {
					val.splice(l, 1);
					flagArray.splice(l, 1);
				}
			}
			//}--
			
			//{--Change unary "+" and "-" operators to the Positive and Negative operators.
			val = checkPositiveNegativeOperators(val, flagArray);
			//}--
			
			//{--If either the leftmost binary operator has no left argument or the rightmost operator has no right argument, throw an error.
			try { //'try' is used in the event that val == [].
				if (flagArray[0].isOperator && !MiscFunctions.isIn(val[0], UnaryOperators)) {
					throw(new Error('Invalid syntax: You entered the "' + MiscFunctions.displayString(val[0]) + '" operator, but did not give it an argument on its left: ' + MiscFunctions.displayString(expression)));
				}
				if (flagArray[flagArray.length - 1].isOperator) {
					throw(new Error('Invalid syntax: You entered the "' + MiscFunctions.displayString(val[val.length - 1]) + '" operator, but did not give it an argument on its right: ' + MiscFunctions.displayString(expression)));
				}
			}
			catch (e:Error) { }
			//}--
			
			MiscFunctions.display("split operators:", val);
			return [val, flagArray];
		}
		
		/**
		 * Function that deals with arguments, throwing errors, calling [i]parseFunctionArgs[/i] or recursions of [i]generateValArray[/i] when needed.
		 * @param	arg	Argument to parse.
		 * @param	propObject	
		 * @return	The parsed argument. In the case a variable or literal, nothing is done. In the case of a function, calls [i]parseFunctionArgs[/i]. In the case of a parenthesised expression, calls [i]generateValArray[/i].
		 */
		private static function parseArg(arg:String):* {
			//{--Remove space at beginning and at end of argument.
			if (arg.charAt(0) == " ") {
				arg = arg.substring(1);
			}
			if (arg.charAt(arg.length - 1) == " ") {
				arg = arg.substring(0, arg.length - 1);
			}
			//}--
			
			//{--If the first part of the argument is a variable, check if that variable is the only thing in 'arg' and, if so, return it.
			if (arg.charAt(0) == VARIABLE) {
				//{--Figure out where the variable's name ends, and place that index, plus 1, in a variable named 'counter'.
				/**
				 * Holds the index of end of the variable's name, plus 1.
				 */
				var counter:int = 1;
				if (MiscFunctions.isIn(arg.charAt(1), VarBeginChars)) {
					counter++;
					while (MiscFunctions.isIn(arg.charAt(counter), VarChars)) {
						counter++;
					}
				}
				//}--
				
				//{--If the var's name isn't the start and end of the argument, it means more than 1 argument was given. Throw an error.
				if (counter != arg.length) {
					throw(new Error('Invalid syntax: 2 or more arguments are specified, but no operation is performed between them: "' + MiscFunctions.displayString(arg) + '"'));
				}
				//}--
				
				return arg;
			}
			//}--
			
			//{--If the first part of the argument is a function, check if that function and its arguments are the only thing in 'arg' and, if so, return it.
			else if (arg.charAt(0) == FUNCTION) {
				/**
				 * Holds the number of parentheses that were opened but not closed.
				 */
				var numberOfParentheses:int = 0;
				/**
				 * Holds the name of the function.
				 */
				var functionName:String;
				/**
				 * Holds the endex at which the function's arguments start, the opening parenthesis not included.
				 */
				var argumentStartIndex:int;
				
				//{--Figure out where the function's name ends, and place that index, plus 1, in a variable named 'counter'. Also place the function's name in 'functionName'.
				/**
				 * Holds the index of end of the variable's name, plus 1.
				 */
				var counter:int = 1;
				if (MiscFunctions.isIn(arg.charAt(1), VarBeginChars)) {
					counter++;
					while (MiscFunctions.isIn(arg.charAt(counter), VarChars)) {
						counter++;
					}
					functionName = arg.substring(0, counter);
				}
				//}--
				
				//{--If the function was named but not called, throw an error. NodyCode does not, to date, support function objects as such.
				if (arg.charAt(counter) != ParenthesisOpen) {
					throw(new Error('Invalid syntax: The "' + MiscFunctions.displayString(arg.substring(0, counter)) + '" function was given but not called with "()": "' + MiscFunctions.displayString(arg.substring(0)) + '"'));
				}
				//}--
				
				argumentStartIndex = counter + 1;
				
				//{----Check if the function's name and its arguments end at the end of the 'arg' parameter.
				//{--Figure out at what index the block of arguments ends and place that index in 'counter'.
				counter++;
				numberOfParentheses++;
				while (numberOfParentheses != 0 && counter < arg.length) {
					if (arg.charAt(counter) == ParenthesisOpen) {
						numberOfParentheses++;
					}
					else if (arg.charAt(counter) == ParenthesisClose) {
						numberOfParentheses--;
					}
					counter++;
				}
				//}--
				
				//{---There are two cases in which an error occurs.
				//{--1: If there was 1 or more parentheses that were opened, but not closed.
				if (numberOfParentheses != 0) {
					throw(new Error('Invalid syntax: Function parentheses were opened but not closed: "' + MiscFunctions.displayString(arg) + '"'));
				}
				//}--
				
				//{--2: If the the argument block ends before the end of the argument.
				if (counter != arg.length) {
					throw(new Error('Invalid syntax: 2 or more arguments are specified, but no operation is performed between them: "' + MiscFunctions.displayString(arg) + '"'));
				}
				//}--
				//}---
				//}----
				
				//{---If there is no error to report, use a functionVal to parse the function's arguments.
				return new FunctionVal(arg.substring(1, argumentStartIndex - 1), arg.substring(argumentStartIndex, arg.length - 1));
				//}--
				
			}
			//}--
			
			//{--If the first part of the argument is a nestable, either recurse it (if parentheses) or use the NCList constructor (if brackets).
			if (MiscFunctions.isIn(arg.charAt(0), NESTABLEOPEN)) {
				
				//{--If the var's name isn't the start and end of the argument, it means more than 1 argument was given. Throw an error.
				if (MiscFunctions.getNestableEndIndex(arg, 0) != arg.length - 1) {
					throw(new Error('Invalid syntax: 2 or more arguments are specified, but no operation is performed between them: "' + MiscFunctions.displayString(arg) + '"'));
				}
				//}--
				
				//{--Deal with each nestable type.
				switch(arg.charAt(0)) {
					//{--If nestable is a parenthesis
					case ParenthesisOpen: {
						var tempArray:Array = generateValArray(arg.substring(1, arg.length - 1));
						
						return tempArray.length == 1 ? tempArray[0] : tempArray; //Avoid parentheses containing only parentheses.
						break;
					}
					//}--
					
					//{--If nestable is a list
					case BracketOpen: {
						return new NCList(arg);
						break;
					}
					//}--
				}
				//}--
				
			}
			//}--
			
			//{--If the first part of the argument is a string-opening character, use the NCString constructor.
			if (MiscFunctions.isIn(arg.charAt(0), QUOTES)) {
				
				//{--If the var's name isn't the start and end of the argument, it means more than 1 argument was given. Throw an error.
				if (MiscFunctions.getStringEndIndex(arg, 0) != arg.length - 1) {
					throw(new Error('Invalid syntax: 2 or more arguments are specified, but no operation is performed between them: "' + MiscFunctions.displayString(arg) + '"'));
				}
				//}--
				
				//{--Return the NodyCode String representing arg.
				return new NCString(arg.substring(1, arg.length - 1));
				//}--
				
			}
			//}--
			
			return arg;
		}
		
		/**
		 * For each "+" or "-" operator, check whether it is to be interpreted as a unary operator. If so,
		 * replace the "+" or "-" with the arbitrary characters, respectively, [i]Positive[/i] and [i]Negative[/i].
		 * 
		 * For each "." found, check whether it is to be interpreted as the decimal point or the dot operator.
		 * @param	val	Input Array.
		 * @param	flagArray	Flag Array.
		 * @return	The [i]val[/i] Array where "-", "+" and "." are replaced.
		 */
		private static function checkPositiveNegativeOperators(val:Array, flagArray:Array):Array {
			/**
			 * No, [i]AS[/i] does not mean ActionScript ☺.
			 * It holds an array containing the [i]Add[/i] and [i]Subtract[/i] operators.
			 */
			var AS:Array = [Add, Subtract];
			/**
			 * Holds the [i]Positive[/i] and [i]Negative[/i] operators.
			 */
			var PN:Array = [Positive, Negative];
			
			try { //'try' is used to prevent error when val == [].
				if (flagArray[0].isOperator && MiscFunctions.isIn(val[0], AS)) { //Only done in a separate structure for optimization purposes. It avoids checking i == 0 each time.
					//{--If the first element is a "+" or "-" operator, replace it. It is unary.
					val[0] = PN[AS.indexOf(val[0])];
					//}--
					
				}
			}
			catch (e:Error) { }
			
			for (var i:int = 1; i < val.length; i++) {
				//{--If the i-th element of val is "+", "-" or "."
				if (flagArray[i].isOperator && MiscFunctions.isIn(val[i], AS.concat(DecimalPoint))) {
					//{--If an operator precedes the "+" or "-" operator, replace "+" or "-" by Positive or Negative.
					if (flagArray[i - 1].isOperator && MiscFunctions.isIn(val[i], AS)) {
						val[i] = PN[AS.indexOf(val[i])];
					}
					//}--
					
				}
				//}--
				
			}
			
			return val;
		}
		
		
		/**
		 * Searches all strings in [i]val[/i] for any of the operators in [i]Operators[/i]. Then changes the value of [i]val[/i] accordingly, in accord with custom specifications needed to run the [i]evaluate[/i] function.
		 * 
		 * For example, given val = [2.5, +, 3, //, [4, -, [9, *, 6]], -, 3, +, [54.6, %, 2]] and Operators = [*, /, %, //], the returned value will be [2.5, +, [3, //, [4, -, 3, + [9, *, 6]]], -, [54.6, %, 2]] .
		 * @param	val	Input array to be scanned.
		 * @param	Operators	Array containing the operators to search for.
		 * @return	[[i]val[/i], [i]flagArray[/i]] after modifications.
		 */
		private static function checkOperator(val:Array, flagArray:Array, Operators:Array):Array {
			
			/**
			 * Let us first of define cases which should cause errors, and others which should not.
			 * 
			 * 
			 * 1) An operator may be adjacent to another in some cases:
			 * 
			 * i  -	[Arg, BinOP, BinOP, Arg]	is NOT	POSSIBLE,	because the second binary operator has no left argument.
			 * ii -	[Arg, BinOP, UnaryOP, Arg]	is NOT	POSSIBLE,	however, if the UnaryOP has a higher precedence, then the val array will already be [Arg, BinOP, [UnaryOP, Arg]]. And thus, [UnaryOP, Arg] is the binary operator's second argument.
			 * iii-	[Arg, UnaryOP, BinlOP, Arg]	is NOT	POSSIBLE,	because the leftmost argument is not associated with any operator, and because the binary operator has no left argument.
			 * iv -	[UnaryOP, UnaryOP, Arg]		is		POSSIBLE, 	because [UnaryOP, Arg] may be used as an argument of the first unary operator.
			 */
			
			//{--If operators are binary
			if (MiscFunctions.isIn(Operators[0], BinaryOperators)) {
				for (var i:int = 0; i < val.length; i++) {
					//{--If val[i] is a string
					if (val[i] is String) {
						//{--If val[i] is one of the sought operators.
						if (flagArray[i].isOperator && MiscFunctions.isIn(val[i], Operators)) {
							
							//{---Place the first argument of operator and the operator itself in tempArray.
							//{--According to 1.i) and 1.iii), a boolean operator should have no operator on its left.
							if (flagArray[i - 1].isOperator) {
								throw(new Error('Invalid syntax: "' + MiscFunctions.displayString(val[i]) + '" should have an argument on its left, not an operator (you entered "' + MiscFunctions.displayString(val[i - 1]) + '")'));
							}
							//}--
							
							//{--If the operator is Assign and the 'variable' to assign value to is not a valid variable name, throw an error.
							if (Operators[0] == Assign && !(val[i - 1] is String && val[i - 1].charAt(0) == VARIABLE)) {
								//{--If the value is a NC keyword, throw a correspnding error.
								if (MiscFunctions.isIn(val[i - 1], Keywords)) {
									throw(new Error('Invalid syntax: Cannot assign a value to "' + MiscFunctions.displayString(val[i - 1]) + '". This is a reserved name.'));
								}
								//}--
								
								//{--Else, the value is (probably) a literal. Throw a corrsponding error.
								else {
									throw(new Error('Invalid syntax: Cannot assign a value to a literal: "' + MiscFunctions.displayString(val[i - 1]) + '".'));
								}
								//}--
								
							}
							//}--
							
							/**
							 * Holds operation currently being parsed.
							 */
							var tempArray:Array = [makeLiteral(val[i - 1]), val[i]];
							//}---
							
							
							//{---Place the second argument of operator in tempArray.
							//{--According to 1.i) and 1.ii), a binary operator should have no argument on its right.
							if (flagArray[i + 1].isOperator) {
								throw(new Error('Invalid syntax: "' + MiscFunctions.displayString(val[i]) + '" should have an argument on its right, not an operator (you entered "' + MiscFunctions.displayString(val[i + 1]) + '")'));
							}
							//}--
							
							tempArray.push(makeLiteral(val[i + 1]));
							//}----
							
							
							//{--Implement tempArray in val.
							val.splice(i - 1, 3, tempArray);
							flagArray.splice(i - 1, 3, _ArgumentObject);
							i--;
							//}--
							
						}
						//}--
						
					}
					//}--
					
				}
				
			}
			//}--
			
			//{--If operators are unary
			else {
				for (var i:int = 0; i < val.length; i++) {
					//{--If val[i] is part of the operators.
					if (val[i] is String && MiscFunctions.isIn(val[i], Operators)) {
						//{--According to 1.iii), a unary operator should have no unary operator on its right. Throw an error if it is the case.
						if (flagArray[i + 1].isOperator && MiscFunctions.isIn(val[i + 1], BinaryOperators)) {
							throw(new Error('Invalid syntax: "' + MiscFunctions.displayString(val[i + 1]) + '" has no argument on its left.'));
						}
						//}--
						
						//{--Find the largest continuous stream of unary operators ends, and place that index, plus 1, in 'endIndex'.
						/**
						 * Holds the index of the last successive unary operator found, starting at i, plus 1.
						 */
						var endIndex:int = i + 1;
						while (MiscFunctions.isIn(val[endIndex], UnaryOperators)) {
							endIndex++;
						}
						//}--
						
						//{--To an array named 'tempArray', assign the value of [unOP_1, [unOP_2, [ ... [unOP_n, Arg]]]] and implement it in val.
						/**
						 * Holds the current opeartion.
						 */
						var tempArray:* = [val[endIndex - 1], makeLiteral(val[endIndex])];
						
						for (var j:int = endIndex - 2; j >= i; j--) {
							tempArray = [val[j], tempArray];
						}
						
						val.splice(i, endIndex - i + 1, tempArray);
						flagArray.splice(i, endIndex - i + 1, _ArgumentObject);
						//}--
						
					}
					//}--
					
				}
				
			}
			//}--
			
			return [val, flagArray];
		}
		
		/**
		 * Takes an object in input, and, if it is a string, tries to coerce it to a literal.
		 * 
		 * 
		 * E.g., '2' -> 2, but '"2"' -> '"2"' and [1, "ヘx"] -> [1, "ヘx"].
		 * @param	val	Input element.
		 * @return	Coerced [i]val[/i] if it could be coerced. If it could not or if
		 * it was not a string, returns the element without modifying it.
		 */
		private static function makeLiteral(val:*):* {
			//{--If val is not a string, no do attempt to make literal.
			if (!(val is String)) {
				return val;
			}
			//}--
			
			switch (val) {
				//{--Try to convert to a bool
				case True: {
					return true;
					break;
				}
				//}--
				
				//{--Try to convert to bool 'false'
				case False: {
					return false;
					break;
				}
				//}--
				
				//{--Try to convert to 'null'
				case None: {
					return null;
					break;
				}
				//}--
				
				//{--Else, try to convert it to a float.
				default: {
					//{--Allow the NaN number.
					if (val == Nan) {
						return NaN;
					}
					//}--
					
					//{--Check if val is written in binary, and, if so, convert it to float.
					if (val.substr(0,  StartBinaryNumber.length) == StartBinaryNumber) {
						return parseInt(val.substring(StartBinaryNumber.length), 2);
					}
					//}--
					
					//{--Else, check if val is written in hex, and, if so, convert it to float.
					if (val.substr(0,  StartHexNumber.length) == StartHexNumber) {
						return parseInt(val.substring(StartHexNumber.length), 16);
					}
					//}--
					
					//{--Else, check if val can be coerced to an float, and, if so, do so.
					if (!isNaN(Number(val))) {
						return Number(val);
					}
					//}--
					
					break;
				}
				//}--
				
			}
			
			//{--If nothing worked, return val.
			return val;
			//}--
			
		}
		
		
		/**
		 * ___________________________
		 * | Evaluation of Val Array |
		 * ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
		 */
		
		/**
		 * Deep-copies and evaluates the value of [i]val[/i], which was earlier generated by the [i]generateValArray[/i] function.
		 * @param	val		Input assertion, in the form of a array with custom specs. If not an array, it has to be a NodyCode literal.
		 * @param	propObject	[i]Properties[/i] instance to get the property values of.
		 * @return	Result of assertion. Can be any data type.
		 */
		public static function evaluate(val:*, propObject:Properties):* {
			return checkArray(MiscFunctions.deepCopy(val), propObject);
		}
		
		/**
		 * Function called by the [i]evaluate[/i] function of the Node.
		 * Is also called by other classes, esp. to check equality between two NodyCode objects.
		 * @param	val	Input assertion, in the form of a array with custom specs.
		 * @return	Result of assertion. Can be any data type.
		 */
		public static function checkArray(val:*, propObject:Properties):* {
			//{--If val is not an array or a functionVal, then it is a literal. Return it.
			if (!(val is Array || val is FunctionVal)) {
				return BuiltinFunctions.valueOf(val, propObject);
			}
			//}--
			
			//{--If val is a functionVal, parse its arguments.
			if (val is FunctionVal) {
				MiscFunctions.display("--funvtionVal's evaluation--", val);
				return val.evaluate(propObject);
			}
			//}--
			
			//{--Replace var names with their values.
			for (var i:int = 0; i < val.length; i++) {
				//{--If operation is binary.
				if (val.length == 3) {
					if (val[1] != Assign) { //If operation is assign, do not replace the var's name by its value.
						val[0] = checkArray(val[0], propObject);
					}
					val[2] = checkArray(val[2], propObject);
				}
				//}--
				
				//{--If operation is unary.
				else if (val.length == 2) {
					val[1] = BuiltinFunctions.valueOf(val[1], propObject);
				}
				//}--
				
			}
			//}--
			
			//{--If operation is binary
			if (val.length == 3) {
				//{--Recurse first argument.
				if (val[0] is Array) {
					val.splice(0, 1, checkArray(val[0], propObject));
				}
				//}--
				
				//{--If operator is boolean, operation can sometimes be optimized. For example, if operation is "val[0] and val[2]" and val[0] is false, then "val[0] and val[2]" is also false, whatever the value of val[2]. Refer: http://www.ee.surrey.ac.uk/Projects/CAL/digital-logic/gatesfunc/index.html
				switch (val[1]) {
					//In this switch, for reasons of simplicity, let A=val[0] and B=val[2].
					
					case And:  {
						//{--If !A then !(A&&B).
						if (!val[0]) {
							return false;
						}
						//}--
						
						//{----Else
						//{--Recursion: evaluate B.
						if (val[2] is Array) {
							val.splice(2, 1, checkArray(val[2], propObject));
						}
						//}--
						
						//{--A&&B = B.
						return val[2];
						//}--
						//}----
						
						break;
					}
					case Or:   {
						//{--If A then A||B.
						if (val[0]) {
							MiscFunctions.display("checkArray(", val, ") =", true);
							return true;
						}
						//}--
						
						//{----Else
						//{--Recursion: evaluate B.
						if (val[2] is Array) {
							val.splice(2, 1, checkArray(val[2], propObject));
						}
						//}--
						
						//{--A||B = B.
						MiscFunctions.display("checkArray(", val, ") =", val[2]);
						return val[2];
						//}--
						//}----
						
						break;
					}
					case Nand: {
						//{--If !A then (!(A&&B)) == true.
						if (!val[0]) {
							MiscFunctions.display("checkArray(", val, ") =", true);
							return true;
						}
						//}--
						
						//{----If A.
						//{--Recursion: evaluate B.
						if (val[2] is Array) {
							val.splice(2, 1, checkArray(val[2], propObject));
						}
						//}--
						
						//{--A&&B = B.
						MiscFunctions.display("checkArray(", val, ") =", !val[2]);
						return !val[2];
						//}--
						//}----
						
						break;
					}
					case Nor:  {
						//{--If A then (!(A||B)) == false.
						if (val[0]) {
							MiscFunctions.display("checkArray(", val, ") =", false);
							return false;
						}
						//}--
						
						//{----Else
						//{--Recursion: evaluate B.
						if (val[2] is Array) {
							val.splice(2, 1, checkArray(val[2], propObject));
						}
						//}--
						
						//{--(!(A||B)) = !B.
						MiscFunctions.display("checkArray(", val, ") =", !val[2]);
						return !val[2];
						//}--
						//}----
						
						break;
					}
					case Xor:  {
						//{----Cannot be optimized.
						//{--Recursion: evaluate B.
						if (val[2] is Array) {
							val.splice(2, 1, checkArray(val[2], propObject));
						}
						//}--
						MiscFunctions.display("checkArray(", val, ") =", (val[0] || val[2]) && !(val[0] && val[2]));
						return (val[0] || val[2]) && !(val[0] && val[2]);
						//}----
						
						break;
					}
					case Xnor: {
						//{----Cannot be optimized.
						//{--Recursion: evaluate B.
						if (val[2] is Array) {
							val.splice(2, 1, checkArray(val[2], propObject));
						}
						//}--
						MiscFunctions.display("checkArray(", val, ") =", !( (val[0] || val[2]) && !(val[0] && val[2]) ));
						return !( (val[0] || val[2]) && !(val[0] && val[2]) );
						//}----
						
						break;
					}
				}
				//}--
				
				//{--Recurse second argument.
				if (val[2] is Array) {
					val.splice(2, 1, checkArray(val[2], propObject));
				}
				//}--
				
				
				//{--If operator is not in the optimizable operators
				switch (val[1]) {
					//{--Algebraic operators
					case Add: {
						//{--Addition of NCLists and NCStrings are equivalent to concatenation.
						if ((val[0] is NCList && val[2] is NCList) || (val[0] is NCString && val[2] is NCString)) {
							val[0].value = val[0].value.concat(val[2].value);
							
							MiscFunctions.display("checkArray(...) =", val[0]);
							return val[0];
						}
						//}--
						
						MiscFunctions.display("checkArray(", val, ") =", val[0] + val[2]);
						return val[0] + val[2];
						break;
					}
					case Subtract: {
						return val[0] - val[2];
						break;
					}
					case Multiply: {
						return val[0] * val[2];
						break;
					}
					case Divide: {
						return val[0] / val[2];
						break;
					}
					case Modulo: {
						return val[0] % val[2];
						break;
					}
					case FloorDivision: {
						return Math.floor(val[0] / val[2]);
						break;
					}
					case Power: {
						return Math.pow(val[0], val[2]);
						break;
					}
					//}--
					
					//{--Non-optimizable Boolean operators
					case Equals: {
						return (MiscFunctions.isNodyCodeClass(val[0]) ? val[0].value : val[0]) == (MiscFunctions.isNodyCodeClass(val[2]) ? val[2].value : val[2]);
						break;
					}
					case DifferentFrom: {
						return BuiltinFunctions.valueOf(val[0], propObject) != BuiltinFunctions.valueOf(val[2], propObject);
						break;
					}
					case LowerThan: {
						return val[0] < val[2];
						break;
					}
					case LowerOrEqualTo: {
						return val[0] <= val[2];
						break;
					}
					case GreaterThan: {
						return val[0] > val[2];
						break;
					}
					case GreaterOrEqualTo: {
						return val[0] >= val[2];
						break;
					}
					//}--
					
					//{--Other Binary Operators
					case Assign: {
						//{--Throw an error if left argument of the assign operator is not a valid variable name.
						if (!(val[0] is String && val[0].charAt(0) == VARIABLE)) {
							throw(new Error('Invalid syntax: Cannot assign a value to a literal or function: ' + MiscFunctions.displayString(val)));
						}
						//}--
						
						MiscFunctions.display(val[0].substring(1), "\t=", val[2]);
						propObject.updateVar(val[0].substring(1), val[2]);
						return;
						
						break;
					}
					case Dot: {
						MiscFunctions.display("Dot operator is used:", val);
						return ClassMethods.dispatchMethod(val[0], val[2].functionName, val[2].argArray);
						
						break;
					}
					//}--
					
					default: {
						throw(new Error('"' + MiscFunctions.displayString(val[1]) + '" is not a valid operator.'));
					}
				}
				//}--
				
			}
			//}--
			
			//{--If operation is unary
			else if (val.length == 2) {
				if (val[1] is Array) {
					val.splice(1, 0, checkArray(val[1], propObject));
					val.splice(2, 1);
				}
				
				switch (val[0]) {
					case Not: {
						MiscFunctions.display("checkArray(", val, ") =", !val[1]);
						return !val[1];
						break;
					}
					case Return: {
						throw(new Error("The 'Return' token has not yet been programmed."));
						break;
					}
					case Negative: {
						MiscFunctions.display("checkArray(", val, ") =", -val[1]);
						return -val[1];
						break;
					}
					case Positive: {
						MiscFunctions.display("checkArray(", val, ") =", val[1]);
						return val[1];
						break;
					}
					default: {
						throw(new Error('"' + MiscFunctions.displayString(val[0]) + '" is not a valid operator.'));
					}
				}
			}
			//}--
			
			
			//{--If val is an empty array, return null.
			if (val.length == 0) {
				return null;
			}
			//}--
			
			//{--If operation has been wrongly parsed, throw an error.
			else {
				throw(new Error("The input array has a subarray with the wrong number of arguments: " + MiscFunctions.displayString(val)));
			}
			//}--
			
		}
		
	}
}