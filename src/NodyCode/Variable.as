package NodyCode {
	
	/**
	 * Defines NodyCode variables.
	 * @author Anthony DENTINGER
	 */
	
	public class Variable {
		
		public var type:String;
		public var value:*;
		
		public function Variable(type:String, value:*) {
			
			this.type = type;
			this.value = value;
			
		}
		
	}

}