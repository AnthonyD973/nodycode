package NodyCode 
{
	import NodyCode.Classes.NCFunction;
	import NodyCode.Variable;
	import Utils.Denominations;
	import Utils.MiscFunctions;
	
	/**
	 * Dynamic class. Instances contain NodyCode properties, i.e.,  variables and functions.
	 * Level objects and functions create their own property objects.
	 * @author Anthony DENTINGER
	 */
	
	public dynamic class Properties 
	{
		private var
		VARIABLE:String = Denominations.VARIABLE,
		FUNCTION:String = Denominations.FUNCTION;
		
		public function Properties() { }
		
		
		/**
		 * Creates a NodyCode object. If object already exists, updates its value.
		 * @param	name	Object's name.
		 * @param	value	Object's value.
		 */
		public function updateVar(name:String, value:* = null):void { //+++
			//{--If object doesn't yet exist, create it.
			if (!this[name]) {
				this[name] = new Variable(BuiltinFunctions.getType(value), value);
				return;
			}
			//}--
			
			//{--Else, if it already exists, just update its value.
			this[name].value = value;
			this[name].type = BuiltinFunctions.getType(value);
			//}--
			
		}
		
		
		/**
		 * Deletes the object of name [i]name[/i].
		 * @param	name	Name of the object to delete.
		 */
		public function deleteObj(name:String):void {
			this[name] = null;
		}
		
		
		
		/**
		 * Returns whether the variable exists in the [i]Properties[/i] instance.
		 * @param	varName	Name of variable.
		 * @return Returns true if [i]varName[/i] exists, else returns false.
		 */
		public function isVar(varName:String):Boolean {
			//{--If property exists and is a variable, return true.
			if (this[varName] && this[varName] is Variable) {
				return true;
			}
			//}--
			
			return false; //Else, return false.
		}
		
		
		/**
		 * Returns whether the function exists in the [i]Properties[/i] instance.
		 * @param	fnName	Name of function.
		 * @return Returns true if [i]fnName[/i] exists, else returns false.
		 */
		public function isFn(fnName:String):Boolean {
			//{--If property exists and is a function, return true.
			if (this[fnName] && this[fnName] is NCFunction) {
				return true;
			}
			//}--
			
			return false; //Else, return false.
		}
		
		
	}
}