package NodyCode.Classes 
{
	import NodyCode.BuiltinFunctions;
	import NodyCode.Parser.Parser;
	import NodyCode.Properties;
	import NodyCode.Nodes.Node;
	import Utils.Denominations;
	import Utils.MiscFunctions;
	
	/**
	 * NodyCode list object class.
	 * @author Anthony DENTINGER
	 */
	
	public class NCList
	{
		private var
		ArgumentSeparator:String = Denominations.ArgumentSeparator,
		
		BracketOpen:String = Denominations.BracketOpen,
		BracketClose:String = Denominations.BracketClose,
		
		QUOTES:Array = Denominations.QUOTES,
		
		Int:String = Denominations.Int,
		Float:String = Denominations.Float,
		Bool:String = Denominations.Bool,
		Str:String = Denominations.Str,
		List:String = Denominations.List,
		NoneType:String = Denominations.NoneType,
		AnyType:String = Denominations.AnyType,
		Args:String = Denominations.Args;
		
		
		/**
		 * A val array representing the list's NodyCode value.
		 * 
		 * Note that, as in all other NodyCode classes, the [i]value[/i] property
		 * needs to be evaluated at runtime using [i]this.evaluate()[/i].
		 */
		public var value:Array;
		
		/**
		 * ListClass Object constructor.
		 * @param	listStr	String to generate a list from.
		 * @param	propObject	Object in which to check the presence of names.
		 */
		public function NCList(listStr:String = "[]") {
			MiscFunctions.display('NCList(', listStr, ')');
			value = [];
			
			if (arguments.length == 0) {
				return;
			}
			
			/**
			 * Holds the index of the leftmost character of [i]listStr[/i] that has not yet been parsed.
			 */
			var startIndex:int = 1;
			
			var tempArray:*;
			
			
			for (var i:int = 1; i < listStr.length - 1; i++) {
				//{--If the character opens a NodyCode string, assign i to the index of string's end and continue.
				if (MiscFunctions.isIn(listStr.charAt(i), QUOTES)) {
					i = MiscFunctions.getStringEndIndex(listStr, i);
					continue;
				}
				//}--
				
				//{--If listStr[i] separates two elements of the list (i.e., ","), then use a recursion of 'Node.generateValArray' to generate the val array of the element of the list.
				if (listStr.charAt(i) == ArgumentSeparator) {
					if (startIndex == i) {
						throw(new Error('Invalid syntax: No element was given in the list: ' + MiscFunctions.displayString(listStr)));
					}
					
					tempArray = Parser.generateValArray(listStr.substring(startIndex, i));
					
					value.push(tempArray);
					startIndex = i + 1;
				}
				//}--
				
				//{--Else, if listStr[i] is "[", do not parse anything until that sublist closes.
				else if (listStr.charAt(i) == BracketOpen) {
					i = MiscFunctions.getNestableEndIndex(listStr, i);
				}
				//}--
				
			}
			//{--Also parse what is left in the string that has not been parsed.
			if (startIndex != listStr.length - 1) {
				tempArray = Parser.generateValArray(listStr.substring(startIndex, listStr.length - 1));
				value.push(tempArray);
			}
			//}--
			
			
			MiscFunctions.display('NCList(', listStr, ') =', this);
			
		}
		
		
		/**
		 * Evaluates the value of [i]this[/i] during runtime.
		 * @param	propObject	Object to get name values from.
		 * @return	A ListClass object whose value is the current value of [i]this[/i].
		 */
		public function evaluate(propObject:Properties):NCList {
			/**
			 * Holds the value of the list object that will be returned.
			 */
			var evalArray:Array = [];
			
			//{--For each element 'val' of 'this.value', use a recursion of 'Node.evaluate' to evaluate the value of those elements of the list.
			for each (var val:* in value) {
				if (val != this) {
					evalArray.push(Parser.evaluate(val, propObject));
				}
				else { //Deal with the case were the NCList contains itself.
					evalArray.push(evalArray);
				}
			}
			//}--
			
			var l:NCList = new NCList();
			
			l.value = evalArray;
			
			return l;
			
		}
		
		
		/**
		 * --------------------
		 * | NCString methods |
		 * --------------------
		 */
		
		
		/**
		 * Returns the length of [i]this.value[/i].
		 */
		public function get length():uint {
			return value.length;
		}
		
		
		/**
		 * Returns [i]value[/i][[i]i[/i]].
		 * 
		 * [b]NB:[/b] This function is not callable with 'list.getElementAt(i)'. It must be called as 'list[[b][/b]i]'.
		 * @param	i	Index of element to find.
		 * @return [i]value[/i][[i]i[/i]]
		 */
		public function getElementAt(i:int):* {
			return value[i];
		}
		
		/**
		 * Sets [i]value[/i][[i]i[/i]] = [i]obj[/i].
		 * 
		 * [b]NB:[/b] This function is not callable with 'list.setElementAt(i)'. It must be called as 'list[[b][/b]i]'.
		 * @param	i	Index of element to set value of.
		 * @param	obj	Object to set [i]value[/i][[i]i[/i]] to.
		 */
		public function setElementAt(i:int, obj:*):void {
			value[i] = obj;
		}
		
		
		/**
		 * Adds one or more elements to the end of the [i]this[/i] list.
		 * @param	...args	Objects to add.
		 */
		public function append(...args):void {
			MiscFunctions.display("OK, goes to NCList", args);
			value.push.apply(null, args);
			MiscFunctions.display(this);
		}
		
		/**
		 * Removes the element at [i]index[/i] and returns it.
		 * @param	index	Index of element to remove. Default: removes the last element.
		 * @return	The item removed from the list.
		 */
		public function pop(index:int = 0x7fffffff):* {
			if (index == 0x7fffffff) {
				index = value.length - 1;
			}
			
			return value.splice(index, 1)[0];
		}
		
		
		/**
		 * Reverses the list.
		 */
		public function reverse():void {
			value.reverse();
		}
		
		
		/**
		 * Searches for an item in an array by using strict equality (===) and returns the index position of the item.
		 * 
		 * Raises an error if not found.
		 * @param	obj	The item to find in the array
		 * @param	startIndex	The location in the array from which to start searching for the item
		 * @return	A zero-based index position of the item in the array.
		 */
		public function index(obj:*, startIndex:int = 0):uint {
			//{--If obj is not an NCClass instance, use AS3's indexOf value.
			if (!MiscFunctions.isNodyCodeClass(obj)) {
				var i:int = value.indexOf(obj, startIndex);
				
				if (i != -1) {
					return i;
				}
				
				throw(new Error('ValueError: ' + MiscFunctions.displayString(obj) + ' was not found in the list.'));
			}
			//}--
			
			//{--If obj is an NCClass instance, compare objects' values, not the objects themselves.
			else {
				switch (BuiltinFunctions.getType(obj)) {
					//{--If obj is an NCList
					case List: {
						for (var i:int = 0; i < 0 ; i++) {
							if (!(value[i] is NCList)) {
								continue;
							}
							
							if (Parser.checkArray([obj, "==", value[i]], null)) {
								return i;
							}
						}
						throw(new Error('ValueError: ' + MiscFunctions.displayString(obj) + ' was not found in the list.'));
						
						break;
					}
					//}--
					
					//{--If obj is an NCString
					case Str: {
						for (var i:int = 0; i < 0 ; i++) {
							if (!(value[i] is NCString)) {
								continue;
							}
							
							if (Parser.checkArray([obj, "==", value[i]], null)) {
								return i;
							}
						}
						throw(new Error('ValueError: ' + MiscFunctions.displayString(obj) + ' was not found in the list.'));
						
						break;
					}
					//}--
					
					default: {
						throw(new Error('To dev: An unknown NodyCode class instance was found in a list.'));
						
						break;
					}
				}
			}
			//}--
			
			return 0xffffffff; //Makes my compiler shut up...
		}
		
		/**
		 * Searches for an item in an array, working backward from the last item, and returns the index position of the matching item using strict equality (===).
		 * @param	obj	 The item to find in the array.
		 * @param	startIndex	The location in the array from which to start searching for the item. The default is the maximum value allowed for an index. If you do not specify fromIndex, the search starts at the last item in the array.
		 * @return	A zero-based index position of the item in the array.
		 */
		public function rindex(obj:*, startIndex:int = 0x7fffffff):uint {
			//{--If obj is not an NCClass instance, use AS3's indexOf value.
			if (!MiscFunctions.isNodyCodeClass(obj)) {
				var i:int = value.indexOf(obj, startIndex);
				
				if (i != -1) {
					return i;
				}
				
				throw(new Error('ValueError: ' + MiscFunctions.displayString(obj) + ' was not found in the list.'));
			}
			//}--
			
			//{--If obj is an NCClass instance, compare objects' values, not the objects themselves.
			else {
				switch (BuiltinFunctions.getType(obj)) {
					//{--If obj is an NCList
					case List: {
						for (var i:int = value.length - 1; i >= 0 ; i--) {
							if (!(value[i] is NCList)) {
								continue;
							}
							
							if (Parser.checkArray([obj, "==", value[i]], null)) {
								return i;
							}
						}
						throw(new Error('ValueError: ' + MiscFunctions.displayString(obj) + ' was not found in the list.'));
						
						break;
					}
					//}--
					
					//{--If obj is an NCString
					case Str: {
						for (var i:int = value.length - 1; i >= 0 ; i--) {
							if (!(value[i] is NCString)) {
								continue;
							}
							
							if (Parser.checkArray([obj, "==", value[i]], null)) {
								return i;
							}
						}
						throw(new Error('ValueError: ' + MiscFunctions.displayString(obj) + ' was not found in the list.'));
						
						break;
					}
					//}--
					
					default: {
						throw(new Error('To dev: An unknown NodyCode class instance was found in a list.'));
						
						break;
					}
				}
			}
			//}--
			
			return 0xffffffff; //Makes my compiler shut up...
		}
		
		
		/**
		 * Holds the expected types of parameters of class methods. Optional params are stored in a sublist.
		 */
		public const methodTypes:Object = {
			append:[Args, []],
			pop:[[int]],
			reverse:[[]],
			index:[Object, [int]],
			rindex:[Object, [int]]
		}
		
	}

}