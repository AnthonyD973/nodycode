package NodyCode.Classes 
{
	import NodyCode.BuiltinFunctions;
	import NodyCode.Properties;
	import Utils.Denominations;
	import Utils.MiscFunctions;
	
	/**
	 * NodyCode string object class.
	 * @author Anthony DENTINGER
	 */
	
	public class NCString 
	{
		private var
		Int:String = Denominations.Int,
		Float:String = Denominations.Float,
		Bool:String = Denominations.Bool,
		Str:String = Denominations.Str,
		List:String = Denominations.List,
		NoneType:String = Denominations.NoneType,
		AnyType:String = Denominations.AnyType,
		Args:String = Denominations.Args;
		
		
		/**
		 * Value of the string.
		 */
		public var value:String;
		
		/**
		 * NCString constructor.
		 * @param	expression	String's value. It should contain only the value, and not the quotes that open it.
		 */
		public function NCString(expression:String = "") {
			value = expression;
		}
		
		/**
		 * Returns the NodyCode string's value.
		 * @param	propObject	Optional properties object. It is useless for a string, and only kept for compatibility purposes.
		 * @return	The NCString itself.
		 */
		public function evaluate(propObject:Properties = null):NCString {
			return this;
		}
		
		
		/**
		 * --------------------
		 * | NCString methods |
		 * --------------------
		 */
		
		/**
		 * Returns [i]value.length[/i].
		 */
		public function get length():int {
			return value.length;
		}
		
		
		/**
		 * A function returning an NCList object whose value is the array returned by [i]this[/i].[i]value[/i].[i]split[/i]([i]separator[/i])
		 * @param	separator	An NCString object.
		 * @return	The split string. An NCList.
		 */
		public function split(separator:NCString):NCList {
			//{--Create an empty NCList and make its value the split NCString.
			var list:NCList = new NCList();
			list.value = value.split(separator.value);
			//}--
			
			//{--Convert the strings into NCStrings
			for (var i:int = 0; i < list.length; i++) {
				list.value[i] = new NCString(list.value[i]);
			}
			//}--
			
			return list;
		}
		
		/**
		 * Joins the [i]list[/i] list, with [i]this.value[/i] as the separator.
		 * 
		 * An error is thrown if one of the elements of [i]list[/i] is not a string.
		 * @param	list	The list to join.
		 * @return	A string consisting of the joined list.
		 */
		public function join(list:NCList):NCString {
			var s:NCString = new NCString();
			
			for (var i:int = 0; i < list.value.length - 1; i++) {
				//{--As in Python, if the element of the list is not a string, throw an error.
				if (!(list.value[i] is NCString)) {
					throw new Error('TypeError: \'' + Str + '\' instance expected, but \'' + BuiltinFunctions.getType(list.value[i]) + '\' was found at list item ' + i);
				}
				//}--
				
				s.value += list.value[i].value + value;
			}
			
			if (!(list.value[list.value.length - 1] is NCString)) {
				throw new Error('TypeError: \'' + Str + '\' instance expected, but \'' + BuiltinFunctions.getType(list.value[i]) + '\' was found at list item ' + i);
			}
			s.value += list.value[list.value.length - 1].value;
			
			return s;
			
		}
		
		
		/**
		 * A function returning an NCString object whose value is the string returned by [i]this[/i].[i]value[/i].[i]toLowerCase[/i]()
		 * @return	The lowercase string. An NCString.
		 */
		public function lower():NCString {
			var str:NCString = new NCString();
			str.value = value.toLowerCase();
			return str;
		}
		
		/**
		 * A function returning an NCString object whose value is the string returned by [i]this[/i].[i]value[/i].[i]toUpperCase[/i]().
		 * @return	The uppercase string. An NCString.
		 */
		public function upper():NCString {
			var str:NCString = new NCString();
			str.value = value.toUpperCase();
			return str;
		}
		
		
		/**
		 * Searches the string and returns the position of the first occurrence of [i]substr[/i] found at or after [i]startIndex[/i] within the calling string.
		 * 
		 * Raises an error if not found.
		 * @param	substr	The substring for which to search.
		 * @param	startIndex	An optional integer specifying the starting index of the search.
		 * @return	The index of the first occurrence of the specified substring.
		 */
		public function index(substr:NCString, startIndex:int = 0):uint {
			var i:int = value.indexOf(substr.value, startIndex);
			
			if (i != -1) {
				return i;
			}
			
			throw(new Error('ValueError: Substring ' + MiscFunctions.displayString(substr) + ' not found in ' + MiscFunctions.displayString(this)));
			
			return 0x7fffffff;
		}
		
		/**
		 * Searches the string from right to left and returns the index of the last occurrence of [i]substr[/i] found before [i]startIndex[/i].
		 * 
		 * Raises an error if not found.
		 * @param	substr	The substring for which to search.
		 * @param	startIndex	An optional integer specifying the starting index of the search.
		 * @return	The index of the first occurrence of the specified substring.
		 */
		public function rindex(substr:NCString, startIndex:int = 0x7fffffff):uint {
			
			var i:int = value.lastIndexOf(substr.value, startIndex);
			
			if (i != -1) {
				return i;
			}
			
			throw(new Error('ValueError: Substring ' + MiscFunctions.displayString(substr) + ' not found in ' + MiscFunctions.displayString(this)));
			
			return 0x7fffffff;
		}
		
		
		/**
		 * Holds the expected types of parameters of class methods. Optional params are stored in a sublist.
		 */
		public const methodTypes:Object = {
			split:[NCString, []],
			join:[NCList, []],
			lower:[[]],
			upper:[[]],
			index:[NCString, [int]],
			rindex:[NCString, [int]]
		}
		
	}

}