package NodyCode.Classes {
	
	import NodyCode.Properties;
	
	/**
	 * NodyCode function object class.
	 * @author Anthony DENTINGER
	 */
	
	public class NCFunction {
		
		/**
		 * Object that holds the variables and functions local to the NodyCode Function.
		 */
		public var properties:Properties;
		
		
		public function NCFunction() {
			properties = new Properties();
		}
		
		
		public function evaluate():* {
			throw (new Error('Function evaluation has not yet been programmed.'));
			
			return;
		}
		
	}

}