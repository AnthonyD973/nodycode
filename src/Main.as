package
{
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import net.flashpunk.World;
	import Worlds.Level;
	import Worlds.MainMenu;
	import Utils.Denominations;
	import Utils.MiscFunctions;

	/**
	 * ...
	 * @author Anthony DENTINGER
	 */
	[Frame(factoryClass="Preloader")]
	public class Main extends Engine {
		
		public var mainMenuObject:MainMenu;
		public var levelObject:Level;
		
		public static var currentWorldName:String;
		
		
		public function Main() {
			MiscFunctions.display("プ"); //Makes the trace output look more consistent.
			
			super(900, 600, 60, false);
			FP.screen.color = 0x000000;
			
			Denominations.generateKeywordValues();
			
			FP.world = FP.mainMenu = new MainMenu();
			FP.level = new Level();
			
		}

	}

}