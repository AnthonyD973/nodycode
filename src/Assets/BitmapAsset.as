package Assets 
{
	import net.flashpunk.graphics.Image;
	/**
	 * ...
	 * @author Anthony DENTINGER
	 */
	public class BitmapAsset 
	{
		[Embed(source = "../../embed/Bitmaps/Arrow.png")] private static const arrowClass:Class;
		public static const arrow:Image = new Image(arrowClass);
		
		
		[Embed(source = "../../embed/Bitmaps/IF.png")] private static const IFClass:Class;
		public static const IFNodeBitmap:Image = new Image(IFClass);
		
		[Embed(source = "../../embed/Bitmaps/COND.png")] private static const CONDClass:Class;
		public static const CONDNodeBitmap:Image = new Image(CONDClass);
		
		public function BitmapAsset() 
		{
			
		}
		
	}

}