package Entities 
{
	import NodyCode.Nodes.Node;
	import flash.ui.MouseCursor;
	import net.flashpunk.Entity;
	import net.flashpunk.utils.Input;
	import Utils.CollisionTypes;
	/**
	 * ...
	 * @author Anthony DENTINGER
	 */
	public class MouseHitbox extends Entity
	{
		public static var dragging:Node = null;
		
		public function MouseHitbox()
		{
			super()
			
			setHitbox(1, 1);
			
			type = CollisionTypes.mouse;
		}
		
		override public function update():void
		{
			if (collide(CollisionTypes.button, x, y))
			{
				Input.mouseCursor = MouseCursor.BUTTON;
			}
			else
				Input.mouseCursor = MouseCursor.ARROW;
			
			x = Input.mouseX;
			y = Input.mouseY;
		}
	}

}