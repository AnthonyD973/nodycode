package Entities 
{
	import flash.ui.MouseCursor;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.utils.Input;
	import Utils.CollisionTypes;
	/**
	 * ...
	 * @author Anthony DENTINGER
	 */
	public class Button extends Entity
	{
		private var _text:Text;
		
		private var _color1:uint;
		private var _color2:uint;
		
		/**
		 * Variable that is true when mouse has clicked on the button and has not yet been released.
		 */
		public var buttonActive:Boolean = false;
		public const onPressed:Function;
		
		/**
		 * Button constructor.
		 * @param	text 	Text String
		 * @param	x		x-position of button.
		 * @param	y		y-position of button.
		 * @param	onPressed	Function to perform when button is pressed.
		 * @param	color1	Default color of text.
		 * @param	color2	Hovered colored of text.
		 * @param 	font	Font of the button's text.
		 */
		public function Button(text:String = null, x:Number = 0, y:Number = 0, onPressed:Function = null, color1:uint = 0x008800, color2:uint = 0x00ff00, size:uint = 0, font:String = null) {
			this._color1 = color1;
			this._color2 = color2;
			this.onPressed = onPressed;
			
			_text = new Text(text, 0, 0);
			_text.color = _color1;
			if (font)
				_text.font = font;
			if (size != 0)
				_text.size = size;
			
			setHitbox(_text.width, _text.height);
			
			type = CollisionTypes.button;
			name = text;
			
			
			super(x, y, _text);
		}
		
		override public function update():void {
			//--If mouse hovers button
			if (collide(CollisionTypes.mouse, x, y))
			{
				_text.color = _color2;
				
				//--Run 'onPressed' when mouse releases the button and is still on the button.
				if (Input.mouseReleased && this.buttonActive) {
					onPressed();
					this.buttonActive = false; //Just to make sure 'onPressed' does not run several times in a row.
				}
				//--
				
				//--Make button active (i.e., ready to call 'onPressed') when mouse presses on the button.
				else if (Input.mousePressed) {
					this.buttonActive = true;
				}
				//--
			}
			//--
			
			//--If mouse does not hover button
			else
			{
				Input.mouseCursor = MouseCursor.ARROW;
				_text.color = _color1;
				this.buttonActive = false; //Make button not active, i.e., not in a state to run the 'onPressed' function.
			}
			//--
		}
		
	}

}