package Worlds 
{
	import Entities.Button;
	import NodyCode.BuiltinFunctions;
	import NodyCode.Classes.NCList;
	import NodyCode.Classes.NCString;
	import NodyCode.Nodes.ConditionNode;
	import NodyCode.Nodes.IfNode;
	import flash.events.Event;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import NodyCode.Nodes.Node;
	import NodyCode.Parser.ClassMethods;
	import NodyCode.Parser.FunctionVal;
	import NodyCode.Parser.Parser;
	import NodyCode.Properties;
	import Utils.MiscFunctions;
	import Entities.MouseHitbox;
	import flash.utils.getTimer;
	
	/**
	 * ...
	 * @author Anthony DENTINGER
	 */
	public class Level extends World {
		
		public var backToMenuButton:Button;
		public var Cond:ConditionNode;
		public var If:IfNode;
		/**
		 * Object that holds the variables and functions of [i]Level[/i] during runtime.
		 */
		public var properties:Properties;
		
		
		public function Level() {
			super();
			
			properties = new Properties();
			
			backToMenuButton = new Button("Back to Menu", 10, 570, BackToMenu);
			
			Cond = new ConditionNode(properties, 500, 0);
			If = new IfNode(properties, 0, 0);
			
			add(new MouseHitbox());
			add(If);
			add(Cond);
			add(backToMenuButton);
			
			run("i = 9");
			run("f = 1");
			run("b = True != False");
			run("s = ' and '");
			run("l = ['spam', 'eggs', 'bacon', 'ham', 'lobster turmidor']");
			run("param1 = -3");
			
			MiscFunctions.display("------------------------------\n");
			
			var val:* = Parser.generateValArray("l = l + [5]");
			
			MiscFunctions.display("-------\nvalueArray:", val, "\n-------");
			
			MiscFunctions.display("-------\nFinal returned value:", Parser.evaluate(val, properties));
			
		}
		
		
		/**
		 * Parses and runs [i]expression[/i].
		 * @param	expression	A string. More or less equivalent to a line of code.
		 * @return	The value returned by running [i]expression[/i].
		 * For example, "[1, 2]" returns the NCList object [1, 2]. But "i = 5" returns void.
		 */
		public function run(expression:String):* {
			return Parser.evaluate(Parser.generateValArray(expression), properties);
		}
		
		
		public static function BackToMenu():void {
			FP.world = FP.mainMenu;
		}
		
		
	}

}