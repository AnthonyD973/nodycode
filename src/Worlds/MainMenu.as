package Worlds 
{
	import Assets.BitmapAsset;
	import net.flashpunk.Entity;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import Entities.Button;
	import Entities.MouseHitbox;
	/**
	 * ...
	 * @author Anthony DENTINGER
	 */
	public class MainMenu extends World
	{
		public var playButton:Button = new Button("PLAY", 100, 100, OpenPlayScreen, 0x008800, 0x00ff00, 30);
		public var shopButton:Button = new Button("SHOP", 100, 200, OpenShopScreen, 0x008800, 0x00ff00, 30);
		
		public function MainMenu() 
		{
			super();
			
			add(new MouseHitbox());
			add(playButton);
			add(shopButton);
		}
		
		
		public function OpenPlayScreen():void
		{
			FP.world = FP.level;
		}
		
		public function OpenShopScreen():void
		{
			
		}
		
	}

}