package Utils 
{
	import NodyCode.BuiltinFunctions;
	import NodyCode.Classes.NCFunction;
	import NodyCode.Classes.NCList;
	import NodyCode.Classes.NCString;
	import NodyCode.Parser.FunctionVal;
	import NodyCode.Parser.Parser;
	import NodyCode.Variable;
	import flash.utils.ByteArray;
	/**
	 * Class containing static custom functions useful for the project, but that cannot be called by a user within a NodyCode script.
	 * @author Anthony DENTINGER
	 */
	public class MiscFunctions 
	{
		include "../../embed/Includes/Denominations_Include_As_Static.as"
		
		public function MiscFunctions() {
			
		}
		
		/**
		 * Checks [i]array[/i] for [i]obj[/i] using strict equality.
		 * @param	obj		Object to check presence of in [i]array[/i].
		 * @param	array	Array to look through.
		 * @return	true if [i]obj[/i] was found in [i]array[/i], else returns false.
		 */
		public static function isIn(obj:*, array:Array):Boolean {
			for each (var i:* in array)
			{
				if (obj === i)
					return true;
			}
			return false;
		}
		
		
		
		
		/**
		 * Looks through [i]expression[/i] string searching for any keyword in [i]Keywords[/i].
		 * @param	expression	Expression string to be scanned.
		 * @param	Keywords	Keywords to look for.
		 * @return	Array with subarrays containing [[i]keywordFound[/i], [i]indexOfKeywordBeginning[/i]].
		 */
		public static function searchKeywords(expression:String, Keywords:Array):Array {
			var keywordsFound:Array = [];
			
			for (var j:int = 0; j < expression.length; j++) {
				Loop2: for (var k:int = 0; k < Keywords.length; k++) { //Checks if substring of expression is equal to Keywords[k].
					if (j + Keywords[k].length - 1 >= expression.length)
						continue;
					
					for (var l:int = 0; l < Keywords[k].length; l++) { //Checks if characters of expression and Keywords[k] are different.
						if (expression.charAt(j + l) != Keywords[k].charAt(l))
							continue Loop2;
					}
					
					keywordsFound.push([Keywords[k], j]);
					j += Keywords[k].length - 1;
					break;
				}
			}
			
			return keywordsFound;
		}
		
		
		
		
		/**
		 * Function especially useful for viewing array and sub-array structure. Replaces AS3
		 * tokens by NodyCode tokens. Otherwise, mainly behaves like AS3's [i]trace[/i] function.
		 * @param	args	Input objects.
		 */
		public static function display(...args):void {
			var valsTraceString:String = "";
			for each (var val:* in args)
				valsTraceString += displayString(val, false) + " ";
			trace(valsTraceString.substring(0, valsTraceString.length - 1));
		}
		
		
		/**
		 * Returns the string of input object, where arrays are between square brackets ("[]") and AS3 Keywords are replaced by NodyCode Keywords.
		 * 
		 * Additionally, if [i]removeIdentifiers[/i] is false:
		 * 1) NCList instances' presence are notified between "<>"
		 * 2) NCString instances' presence are notified between "`´"
		 * 3) NCFunction instances' presence are notified between "##"
		 * 4) Variable instances' presence are notified between "¬¬"
		 * 
		 * @param	val	Object to turn into a string.
		 * @param	removeIdentifiers	Whether to remove the arbitrary identifiers and replace the other arbitrary
		 * characters by their usual equivalent (such as replacing by "-" the unary operator represented internally as "プ").
		 * @return	A string representing the [i]val[/i] object.
		 */
		public static function displayString(val:*, removeIdentifiers:Boolean = true):String {
			var traceString:String = new String();
			
			//{--Pretty-display arrays.
			if (val is Array) {
				traceString += BracketOpen;
				
				for (var i:int = 0; i < val.length - 1; i++) {
					if (val[i] is NCList && val === val[i].value) {
						traceString += "<" + BracketOpen + "..." + BracketClose + ">" + ArgumentSeparator + " ";
					}
					else {
						traceString += displayString(val[i], removeIdentifiers) + ArgumentSeparator + " ";
					}
					
				}
				traceString += (val.length == 0 ? "" : ((val[i] is NCList && val === val[i].value) || (val === val[i]) ? "<" + BracketOpen + "..." + BracketClose + ">" : displayString(val[i], removeIdentifiers))) + BracketClose;
			}
			//}--
			
			//{--If val is not an array, val might be a string. If so, display val as-is.
			else if (val is String) {
				if (removeIdentifiers) {
					var currentString:* = val.split("\n").join("\\n");
					
					for each (var identifier:String in IDENTIFIERS) {
						currentString = currentString.split(identifier).join("");
					}
					currentString = currentString.split(Positive).join(Add);
					currentString = currentString.split(Negative).join(Subtract);
					traceString += currentString;
				}
				else {
					traceString += val;
				}
			}
			//}--
			
			//{--If val is a NodyCode class, use 'val.value' as text.
			else if (MiscFunctions.isNodyCodeClass(val) || val is Variable) {
				if (!removeIdentifiers) {
					if (val is NCList) {
						traceString += "<" + displayString(val.value, removeIdentifiers) + ">";
					}
					else if (val is NCString) {
						traceString += "`" + displayString(val.value, removeIdentifiers) + "´";
					}
					else if (val is NCFunction) {
						traceString += "#" + displayString(val.value, removeIdentifiers) + "#";
					}
					else if (val is Variable) {
						traceString += "¬" + displayString(val.value, removeIdentifiers) + "¬";
					}
					else {
						throw(new Error('Please add a display convention for ' + String(val)));
					}
				}
				else {
					if (val is NCString) {
						traceString += "'" + displayString(val.value, removeIdentifiers) + "'";
					}
					else {
						traceString += displayString(val.value, removeIdentifiers);
					}
				}
			}
			//}--
			
			//{--Else, replace AS3 keywords with NodyCode ones.
			else {
				if (val is FunctionVal) {
					traceString += "{" + displayString(val.functionName, removeIdentifiers) + ", " + displayString(val.argArray, removeIdentifiers) + "}";
				}
				
				else {
					switch (val) {
						case true: {
							traceString += True;
							break;
						}
						case false: {
							traceString += False;
							break;
						}
						case null: {
							traceString += None;
							break;
						}
						case NaN: {
							traceString += Nan;
							break;
						}
						default: {
							traceString += String(val);
							break;
						}
					}
				}
			}
			//}--
			
			return traceString;
		}
		
		
		
		/**
		 * Checks if [i]obj[/i] has a variable or function identifier.
		 * @param	obj	Input object.
		 * @return Whether [i]obj[/i] is the name of an identifier, that is, has a var/fn identifier.
		 */
		public static function isIdentifierName(obj:*):Boolean {
			return (obj is String && isIn(obj.charAt(0), IDENTIFIERS));
		}
		
		
		/**
		 * Finds the index of end of the nestable argument of [i]expression[/i] that begins at [i]startingAt[/i] and returns that index.
		 * 
		 * Throws an error if the nestable was not completely closed.
		 * @param	expression	The string in which to find the index.
		 * @param	startingAt	Index at which to start the search for nested's end.
		 * Should be the index of the character that opens the nestable.
		 * @return	Index of nestable's end.
		 */
		public static function getNestableEndIndex(expression:String, startingAt:uint):uint {
			//NOTE: From now on, startingAt will be a counter. The name loses its significance.
			
			/**
			 * Holds the number of a certain nestable argument that were opened but not closed.
			 */
			var numberOfNestable:int = 1;
			/**
			 * Holds the character used to open the current nestable argument.
			 */
			var nestableOpenedWith:String = expression.charAt(startingAt);
			/**
			 * Holds the character used to close the current nestable argument.
			 */
			var nestableClosedWith:String = NESTABLECLOSE[NESTABLEOPEN.indexOf(nestableOpenedWith)];
			
			nestableOpenedWith = expression.charAt(startingAt);
			nestableClosedWith = NESTABLECLOSE[NESTABLEOPEN.indexOf(nestableOpenedWith)]; //For example, if nestableOpenedWith == "[", then nestableClosedWith == "]".
			
			//{--Place staringAt at the index of the end of the nestable.
			while (numberOfNestable != 0 && startingAt < expression.length - 1) {
				startingAt++;
				if (expression.charAt(startingAt) == nestableOpenedWith) {
					numberOfNestable++;
					continue;
				}
				if (expression.charAt(startingAt) == nestableClosedWith) {
					numberOfNestable--;
					continue;
				}
			}
			//}--
			
			//{--Throw error if nested was not completely closed.
			if (numberOfNestable != 0) {
				switch (nestableOpenedWith) {
					case BracketOpen: {
						throw(new Error('Invalid syntax: A bracket was opened but not closed: "' + MiscFunctions.displayString(expression) + '"'));
						break;
					}
					
					case ParenthesisOpen: {
						throw(new Error('Invalid syntax: A parenthesis was opened but not closed: "' +  MiscFunctions.displayString(expression) + '"'));
						break;
					}
					
					default: {
						throw(new Error('You (the dev) recently added a new type of nestable argument, but did not implement it in MiscFunctions.getNestableEndIndex: "' + nestableOpenedWith + nestableClosedWith + '"'))
						break;
					}
				}
			}
			//}--
			
			return startingAt;
		}
		
		/**
		 * Finds the index of end of the string of [i]expression[/i] that begins at [i]startingAt[/i] and returns that index.
		 * 
		 * Throws an error if the string was not completely closed.
		 * @param	expression	The string in which to find the index.
		 * @param	startingAt	Index at which to start the search for string's end.
		 * Should be the index of the character that opens the string.
		 * @return	Index of string's end.
		 */
		public static function getStringEndIndex(expression:String, startingAt:uint):uint {
			/**
			 * Holds the character that closes the string.
			 */
			var stringClose:String = expression.charAt(startingAt);
			
			
			//NOTE: From now on, startingAt will be a counter. The name loses its significance.
			
			startingAt++;
			while ((expression.charAt(startingAt) != stringClose || expression.charAt(startingAt - 1) == BackSlash) && startingAt < expression.length) {
				startingAt++;
			}
			
			if (startingAt == expression.length) {
				throw(new Error('Invalid syntax: A string was opened but not closed: ' + MiscFunctions.displayString(expression)));
			}
			
			return startingAt;
			
		}
		
		/**
		 * Finds the index of end of the identifier name of [i]expression[/i] that begins at [i]startingAt[/i] and returns that index.
		 * @param	expression	The string in which to find the index.
		 * @param	startingAt	Index at which to start the search for identifier's end.
		 * That first character should be in [i]VarBeginChars[/i], since this function does not check that condition.
		 * @return	Index of identifier's end.
		 */
		public static function getIdentifierEndIndex(expression:String, startingAt:uint):uint {
			//NOTE: From now on, startingAt will be a counter. The name loses its significance.
			
			startingAt++;
			while (MiscFunctions.isIn(expression.charAt(startingAt), VarChars)) {
				startingAt++;
			}
			
			return startingAt - 1;
		}
		
		/**
		 * Finds the index of end of the number of [i]expression[/i] that begins at [i]startingAt[/i] and returns that index.
		 * @param	expression	The string in which to find the index.
		 * @param	startingAt	Index at which to start the search for number's end.
		 * That first character should be in [i]Digits[/i], since this function does not check that condition.
		 * @return	Index of identifier's end.
		 */
		public static function getNumberEndIndex(expression:String, startingAt:uint):uint {
			//NOTE: From now on, startingAt will be a counter. The name loses its significance.
			
			
			//{--To an array named 'chars', assign the characters used to write numbers in base 2, 10 or 16.
				//{--If the number is binary
				if (expression.substr(startingAt, StartBinaryNumber.length) == StartBinaryNumber) {
					startingAt += StartBinaryNumber.length;
					var chars:Array = BinaryNumberSymbols;
				}
				//}--
				
				//{--Else, if the number is hexadecimal
				else if(expression.substr(startingAt, StartHexNumber.length) == StartHexNumber) {
					startingAt += StartHexNumber.length;
					var chars:Array = HexNumberSymbols;
				}
				//}--
				
				//{--Else, the number is decimal
				else {
					var chars:Array = NumberSymbols;
				}
				//}--
				
			//}--
			
			while (MiscFunctions.isIn(expression.charAt(startingAt), chars)) {
				startingAt++;
			}
			
			return startingAt - 1;
		}
		
		
		/**
		 * Checks whether the object is an instance of a NodyCode class. NodyCode [i]Variables[/i] are excluded from these.
		 * 
		 * See [i]NodyCode.Classes[/i] for an exhaustive list of NodyCode classes.
		 * @param	obj	Object to check.
		 * @return	true if [i]obj[/i] is a NodyCode class instance, else false.
		 */
		public static function isNodyCodeClass(obj:*):Boolean {
			return obj is NCFunction || obj is NCList || obj is NCString;
		}
		
		
		/**
		 * Deep-copies an object into an identical one but independent from [i]obj[/i]. It's probably a bit sluggish, though.
		 * @param	obj	Theoretically, any object instance. In practice, do not use anything other than Arrays,
		 * NCLists, NCFunctions, FunctionVals, or simple AS3 classes as Number and String, without prior tests.
		 * Arrays and NCLists should not contain objects that are not in the former list.
		 * @return	An independant object.
		 */
		public static function deepCopy(obj:*, parentList:NCList = null, newList:NCList = null):* {
			//{--If obj is an Array, clone all of its elements.
			if (obj is Array) {
				var tempArray:Array = [];
				for each(var a:* in obj) {
					tempArray.push(deepCopy(a, parentList, newList));
				}
				return tempArray;
			}
			//}--
			
			//{--If obj is a NodyCode List, create an empty NCList object and clone its value.
			if (obj is NCList) {
				var tempList:NCList = new NCList();
				tempList.value = deepCopy(obj.value, obj, tempList);
				return tempList;
			}
			//}--
			
			//{--If obj is a NodyCode String, create an empty NCString object and copy its value.
			if (obj is NCString) {
				var s:NCString = new NCString();
				s.value = obj.value;
				
				return s;
			}
			//}--
			
			//{--If obj is a FunctionVal, create an empty FunctionVal object and copy its value.
			if (obj is FunctionVal) {
				var f:FunctionVal = new FunctionVal();
				
				f.functionName = obj.functionName;
				f.argArray = deepCopy(obj.argArray);
				
				return f;
			}
			//}--
			
			//{--Finally, if obj was neither an Array nor a NodyCode List, return source as-is.
			return obj;
			//}--
			
		}
		
	}

}