package Utils {
	
	
	
	/**
	 * Defines constants to be used in some classes.
	 * @author Anthony DENTINGER
	 */
	
	public class Denominations {
		
		public static var
		
		//{--Binary Algebraic operators
		Add:String = "+",
		
		Subtract:String = "-",
		
		Power:String = "**",
		
		Multiply:String = "*",
		
		FloorDivision:String = "//",
		
		Divide:String = "/",
		
		Modulo:String = "%",
		//}--
		
		
		//{--Binary Boolean Operators
		Equals:String = "==",
		
		DifferentFrom:String = "!=",
		
		GreaterOrEqualTo:String = ">=",
		
		LowerOrEqualTo:String = "<=",
		
		GreaterThan:String = ">",
		
		LowerThan:String = "<",
		
		And:String = "and",
		
		Or:String = "or",
		
		Xor:String = "xor",
		
		Nand:String = "nand",
		
		Nor:String = "nor",
		
		Xnor:String = "xnor",
		//}--
		
		
		//{--Other Binary Operators
		Assign:String = "=",
		
		Dot:String = ".",
		//}--
		
		
		//{--Unary Operators
		Not:String = "not",
		
		Return:String = "return",
		
		Negative:String = "プ",
		
		Positive:String = "ブ",
		//}--
		
		
		//{--Literal Values
		True:String = "True",
		
		False:String = "False",
		
		None:String = "None",
		
		Nan:String = "NaN",
		//}--
		
		
		//{--Number Characters
		Digits:Array = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
		HexDigits:Array = Digits.concat("a", "b", "c", "d", "e", "f"),
		BinaryDigits:Array = [Digits[0], Digits[1]],
		
		DecimalPoint:String = ".",
		
		StartBinaryNumber:String = "0b",
		StartHexNumber:String = "0x",
		
		/**
		 * All symbols used to write numbers: digits and the decimal point.
		 */
		NumberSymbols:Array = Digits.concat(DecimalPoint),
		HexNumberSymbols:Array = HexDigits.concat(DecimalPoint),
		BinaryNumberSymbols:Array = BinaryDigits.concat(DecimalPoint),
		//}--
		
		
		//{--Node Types
		IF:String = "IF",
		
		FOR:String = "FOR",
		
		WHILE:String = "WHILE",
		//}--
		
		
		//{--Variable identifiers and characters
		VARIABLE:String = "ヘ",
		
		FUNCTION:String = "ベ",
		
		IDENTIFIERS:Array = [VARIABLE, FUNCTION],
		
		/**
		 * A variable name may start with any of these characters. Uppercase characters are also allowed.
		 */
		VarBeginChars:Array = ["_", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],
		
		/**
		 * A variable/function name may contain these characters.
		 */
		VarChars:Array = VarBeginChars.concat("0", "1", "2", "3", "4", "5", "6", "7", "8", "9"),
		//}--
		
		
		//{--Delimiters for literals and function arguments
		Quote:String = '"',
		Apostrophe:String = "'",
		QUOTES:Array = [Quote, Apostrophe],
		
		BackSlash:String = "\\",
		
		BracketOpen:String = "[",
		BracketClose:String = "]",
		
		ParenthesisOpen:String = "(",
		ParenthesisClose:String = ")",
		
		/**
		 * The set of characters that begin a nestable argument, e.g., a list or a parenthesis.
		 */
		NESTABLEOPEN:Array = [BracketOpen, ParenthesisOpen],
		/**
		 * The set of characters that end a nestable argument, e.g., a list or a parenthesis.
		 */
		NESTABLECLOSE:Array = [BracketClose, ParenthesisClose],
		
		ArgumentSeparator:String = ",",
		//}--
		
		
		//{--Object types
		Int:String = "int",
		
		Float:String = "float",
		
		Bool:String = "bool",
		
		Str:String = "str",
		
		List:String = "list",
		
		NoneType:String = "NoneType",
		
		AnyType:String = "*",
		
		Args:String = "args",
		
		ObjectTypes:Array = [Int, Float, Bool, Str, List, NoneType],
		//}--
		
		
		/**
		 * Operators that take in input two numbers, e.g., "+", "//", or "<="
		 */
		AlgebraicOperators:Array = [Add, Subtract, Power, Multiply, FloorDivision, Divide, Modulo, Equals, DifferentFrom, GreaterOrEqualTo, LowerOrEqualTo, GreaterThan, LowerThan],
		
		/**
		 * Operators that take in input two booleans, e.g., "and", "nand" or "xnor".
		 */
		BooleanOperators:Array = [And, Or, Xor, Nand, Nor, Xnor],
		
		
		
		/**
		 * The set of all Binary Operators.
		 */
		BinaryOperators:Array = AlgebraicOperators.concat(Assign, Dot).concat(BooleanOperators),
		
		/**
		 * Operators that only take one number/boolean in input.
		 */
		UnaryOperators:Array = [Not, Return, Negative, Positive],
		
		/**
		 * The set of all operators in NodyCode.
		 */
		Operators:Array = BinaryOperators.concat(UnaryOperators),
		
		
		/**
		 * Operators that are both binary and keywords.
		 */
		BinaryKeywordOperators:Array,
		
		/**
		 * Operators that are both unary and keywords.
		 */
		UnaryKeywordOperators:Array,
		
		/**
		 * Operators that also are keywords.
		 */
		KeywordOperators:Array,
		
		/**
		 * Literal values that are also keywords.
		 */
		KeywordLiterals:Array,
		
		/**
		 * Keywords: Reserved tokens that are not to be used as variable or function names.
		 */
		Keywords:Array,
		
		/**
		 * Operators that are not keywords.
		 */
		NonKeywordOperators:Array;
		
		
		
		public function Denominations() {
			MiscFunctions.display("\n----------------------------------------------------------------------\nDenominations constructor was called. Value of keyword-holding arrays:\n")
			Denominations.BinaryKeywordOperators = Denominations.getBinaryKeywordOperators();
			MiscFunctions.display("BinaryKeywordOperators =", Denominations.BinaryKeywordOperators);
			
			Denominations.UnaryKeywordOperators = Denominations.getUnaryKeywordOperators();
			MiscFunctions.display("UnaryKeywordOperators =", Denominations.UnaryKeywordOperators);
			
			Denominations.KeywordOperators = Denominations.BinaryKeywordOperators.concat(Denominations.UnaryKeywordOperators);
			MiscFunctions.display("KeywordOperators =", Denominations.KeywordOperators);
			
			Denominations.KeywordLiterals = Denominations.getKeywordLiterals();
			MiscFunctions.display("KeywordLiterals =", Denominations.KeywordLiterals);
			
			Denominations.Keywords = Denominations.KeywordOperators.concat(Denominations.KeywordLiterals);
			MiscFunctions.display("Keywords =", Denominations.Keywords);
			
			Denominations.NonKeywordOperators = Denominations.getNonKeywordOperators();
			MiscFunctions.display("NonKeywordOperators =", Denominations.NonKeywordOperators);
			
			MiscFunctions.display("----------------------------------------------------------------------\n")
		}
		
		/**
		 * Checks all binary operators, and returns an array containing those that are keywords,
		 * that is, those which, were they not reserved, could be variable or function names.
		 * @return	An array: the set of Binary Keyword Operators.
		 */
		public static function getBinaryKeywordOperators():Array {
			var bKOps:Array = [];
			for each (var operator:String in BinaryOperators) {
				if (MiscFunctions.isIn(operator.charAt(0), VarBeginChars)) {
					bKOps.push(operator);
				}
			}
			
			return bKOps;
		}
		
		
		/**
		 * Checks all unary operators, and returns an array containing those that are keywords,
		 * that is, those which, were they not reserved, could be variable or function names.
		 * @return	An array: the set of Unary Keyword Operators.
		 */
		public static function getUnaryKeywordOperators():Array {
			var uKOps:Array = [];
			for each (var operator:String in UnaryOperators) {
				if (MiscFunctions.isIn(operator.charAt(0), VarBeginChars)) {
					uKOps.push(operator);
				}
			}
			
			return uKOps;
		}
		
		
		/**
		 * Checks all unary operators, and returns an array containing those that are keywords,
		 * that is, those which, were they not reserved, could be variable or function names.
		 * @return	An array: the set of Unary Keyword Operators.
		 */
		public static function getNonKeywordOperators():Array {
			var nKOps:Array = [];
			for each (var operator:String in Operators) {
				if (!MiscFunctions.isIn(operator.charAt(0), VarBeginChars)) {
					nKOps.push(operator);
				}
			}
			
			return nKOps;
		}
		
		
		/**
		 * Checks all literals, and returns an array containing those that are keywords,
		 * that is, those which, were they not reserved, could be variable or function names.
		 * @return	An array: the set of Keyword Literals.
		 */
		public static function getKeywordLiterals():Array {
			var kLs:Array = [];
			for each (var literal:String in [True, False, None, Nan].concat(ObjectTypes)) {
				if (MiscFunctions.isIn(literal.charAt(0), VarBeginChars)) {
					kLs.push(literal);
				}
			}
			
			return kLs;
		}
		
		
		/**
		 * (Re)checks the operators and literals that are keywords by calling the [i]Denominations[/i] class' constructor.
		 */
		public static function generateKeywordValues():void {
			new Denominations();
		}
		
	}
}