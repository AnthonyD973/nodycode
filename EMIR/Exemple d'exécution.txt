[utiliser encodage UTF-8]
Voici ce que la console de Flashdevelop écrit lors de l'exécution du
code 'l = l + [5]', avec l la liste
["spam", "eggs", "bacon", "ham", "lobster turmidor"] :

SPLITOPERATORSANDARGS'S INPUT:  l=l+[5]
-- i = 0 --
Not a number. []
Not a string. []
Not a nestable. []
Not closing nestable. []
NAME found.        l=l+[5]
-- i = 2 --
Not a number. []
Not a string. []
Not a nestable. []
Not closing nestable. []
Not a name. []
OPERATOR found.        ヘl=l+[5]
-- i = 3 --
Not a number. [ヘl, =]
Not a string. [ヘl, =]
Not a nestable. [ヘl, =]
Not closing nestable. [ヘl, =]
NAME found.        ヘl=l+[5]
-- i = 5 --
Not a number. [ヘl, =]
Not a string. [ヘl, =]
Not a nestable. [ヘl, =]
Not closing nestable. [ヘl, =]
Not a name. [ヘl, =]
OPERATOR found.        ヘl=ヘl+[5]
-- i = 6 --
Not a number. [ヘl, =, ヘl, +]
Not a string. [ヘl, =, ヘl, +]
NESTABLE found. New value of i: 8        ヘl=ヘl+[5]
NCList( [5] )
SPLITOPERATORSANDARGS'S INPUT:  5
-- i = 0 --
split operators: [5]
NCList( [5] ) = <[5]>
split operators: [ヘl, =, ヘl, +, <[5]>]
-------
valueArray: [ヘl, =, [ヘl, +, <[5]>]] 
-------
NCList( [] )
NCList( [] )
NCList( [] )
NCList( [] )
NCList( [] )
NCList( [] )
NCList( [] )
checkArray(...) = <[`spam´, `eggs´, `bacon´, `ham´, `lobster turmidor´, 5]>
NCList( [] )
NCList( [] )
l 	= <[`spam´, `eggs´, `bacon´, `ham´, `lobster turmidor´, 5]> // <==== Instruction réellement effectuée