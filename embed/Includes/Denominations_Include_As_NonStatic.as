private const

//{--Binary Algebraic operators
Add:String = Utils.Denominations.Add,
Subtract:String = Utils.Denominations.Subtract,
Multiply:String = Utils.Denominations.Multiply,
Divide:String = Utils.Denominations.Divide,
Modulo:String = Utils.Denominations.Modulo,
FloorDivision:String = Utils.Denominations.FloorDivision,
Power:String = Utils.Denominations.Power,
//}--


//{--Binary Boolean Operators
Equals:String = Utils.Denominations.Equals,
DifferentFrom:String = Utils.Denominations.DifferentFrom,
GreaterOrEqualTo:String = Utils.Denominations.GreaterOrEqualTo,
LowerOrEqualTo:String = Utils.Denominations.LowerOrEqualTo,
GreaterThan:String = Utils.Denominations.GreaterThan,
LowerThan:String = Utils.Denominations.LowerThan,
And:String = Utils.Denominations.And,
Nand:String = Utils.Denominations.Nand,
Or:String = Utils.Denominations.Or,
Nor:String = Utils.Denominations.Nor,
Xor:String = Utils.Denominations.Xor,
Xnor:String = Utils.Denominations.Xnor,
//}--


//{--Other Binary Operators
Assign:String = Utils.Denominations.Assign,

Dot:String = Utils.Denominations.Dot,
//}--


//{--Unary Operators
Not:String = Utils.Denominations.Not,

Return:String = Utils.Denominations.Return,

Negative:String = Utils.Denominations.Negative,
Positive:String = Utils.Denominations.Positive,
//}--


//{--Literal Values
True:String = Utils.Denominations.True,
False:String = Utils.Denominations.False,

None:String = Utils.Denominations.None,

Nan:String = Utils.Denominations.Nan,
//}--


//{--Number Characters
Digits:Array = Utils.Denominations.Digits,
HexDigits:Array = Utils.Denominations.HexDigits,
BinaryDigits:Array = Utils.Denominations.BinaryDigits,

DecimalPoint:String = Utils.Denominations.DecimalPoint,

StartBinaryNumber:String = Utils.Denominations.StartBinaryNumber,
StartHexNumber:String = Utils.Denominations.StartHexNumber,

/**
 * All symbols used to write numbers: digits and the decimal point.
 */
NumberSymbols:Array = Utils.Denominations.NumberSymbols,
HexNumberSymbols:Array = Utils.Denominations.HexNumberSymbols,
BinaryNumberSymbols:Array = Utils.Denominations.BinaryNumberSymbols,
//}--


//{--Node Types
IF:String = Utils.Denominations.IF,
FOR:String = Utils.Denominations.FOR,
WHILE:String = Utils.Denominations.WHILE,
//}--


//{--Variable identifiers and characters
VARIABLE:String = Utils.Denominations.VARIABLE,
FUNCTION:String = Utils.Denominations.FUNCTION,

IDENTIFIERS:Array = Utils.Denominations.IDENTIFIERS,

VarBeginChars:Array = Utils.Denominations.VarBeginChars,
VarChars:Array = Utils.Denominations.VarChars,
//}--


//{--Delimiters for literals
Quote:String = Utils.Denominations.Quote,
Apostrophe:String = Utils.Denominations.Apostrophe,
QUOTES:Array = Utils.Denominations.QUOTES,

BackSlash:String = Utils.Denominations.BackSlash,

BracketOpen:String = Utils.Denominations.BracketOpen,
BracketClose:String = Utils.Denominations.BracketClose,

ParenthesisOpen:String = Utils.Denominations.ParenthesisOpen,
ParenthesisClose:String = Utils.Denominations.ParenthesisClose,

NESTABLEOPEN:Array = Utils.Denominations.NESTABLEOPEN,
NESTABLECLOSE:Array = Utils.Denominations.NESTABLECLOSE,
ArgumentSeparator:String = Utils.Denominations.ArgumentSeparator,
//}--


//{--Object types
Int:String = Utils.Denominations.Int,
Float:String = Utils.Denominations.Float,
Bool:String = Utils.Denominations.Bool,
Str:String = Utils.Denominations.Str,
List:String = Utils.Denominations.List,
NoneType:String = Utils.Denominations.NoneType,
AnyType:String = Utils.Denominations.AnyType,
Args:String = Utils.Denominations.Args,
ObjectTypes:Array = Utils.Denominations.ObjectTypes,
//}--



AlgebraicOperators:Array = Utils.Denominations.AlgebraicOperators,
BooleanOperators:Array = Utils.Denominations.BooleanOperators,
BinaryOperators:Array = Utils.Denominations.BinaryOperators,
UnaryOperators:Array = Utils.Denominations.UnaryOperators,

BinaryKeywordOperators:Array = Utils.Denominations.BinaryKeywordOperators,
UnaryKeywordOperators:Array = Utils.Denominations.UnaryKeywordOperators,
KeywordOperators:Array = Utils.Denominations.KeywordOperators,
KeywordLiterals:Array = Utils.Denominations.KeywordLiterals,
Keywords:Array = Utils.Denominations.Keywords,
NonKeywordOperators:Array = Utils.Denominations.NonKeywordOperators,

Operators:Array = Utils.Denominations.Operators;